<?php

namespace App\Controller;

use App\Repository\RetailersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FindUsController extends AbstractController
{
    /**
     * @Route("/nous-trouver", name="find_us")
     */
    public function showFindUsPage(RetailersRepository $retailersRepository): Response
    {
        return $this->render('find_us/find_us.html.twig', [
            'retailers' => $retailersRepository->findAll(),
        ]);
    }
}
