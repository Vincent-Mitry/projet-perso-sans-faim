<?php

namespace App\Tests;

use App\Entity\Ingredient;
use App\Entity\Recipe;
use PHPUnit\Framework\TestCase;

class IngredientUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $ingredient = new Ingredient();
        $recipe = new Recipe();

        $ingredient->setName('name')
             ->setQuantity(1.1)
             ->setUnit('g')
             ->setRecipe($recipe);

        $this->assertTrue('name' === $ingredient->getName());
        $this->assertTrue(1.1 === $ingredient->getQuantity());
        $this->assertTrue('g' === $ingredient->getUnit());
        $this->assertTrue($recipe === $ingredient->getRecipe());
    }

    public function testIsFalse()
    {
        $ingredient = new Ingredient();
        $recipe = new Recipe();

        $ingredient->setName('name')
                ->setQuantity(1.1)
                ->setUnit('g')
                ->setRecipe($recipe);

        $this->assertFalse('false' === $ingredient->getName());
        $this->assertFalse(2.2 === $ingredient->getQuantity());
        $this->assertFalse('false' === $ingredient->getUnit());
        $this->assertFalse(new Recipe() === $ingredient->getRecipe());
    }

    public function testIsEmpty()
    {
        $ingredient = new Ingredient();

        $this->assertEmpty($ingredient->getName());
        $this->assertEmpty($ingredient->getQuantity());
        $this->assertEmpty($ingredient->getUnit());
        $this->assertEmpty($ingredient->getRecipe());
    }
}
