<?php

namespace App\Controller\Admin;

use App\Entity\Recipe;
use App\Form\RecipeIngredientsType;
use App\Repository\RecipeRepository;
use App\Service\CsvService;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RecipeCrudController extends AbstractCrudController
{
    private $csvService;
    private $filterFactory;
    
    public function __construct(
        CsvService $csvService,
        FilterFactory $filterFactory
    ) {
        $this->csvService = $csvService;
        $this->filterFactory = $filterFactory;
    }
    
    public static function getEntityFqcn(): string
    {
        return Recipe::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            TextField::new('title')->hideOnIndex(),
            TextField::new('title')->setTemplatePath('/admin/recipe_title_index.html.twig')->onlyOnIndex(),
            AssociationField::new('product'),
            ArrayField::new('steps')->hideOnIndex()->addHtmlContentsToBody(),
            ImageField::new('main_picture')->setBasePath('/uploads/recipe/images')->onlyOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms()->setFormTypeOption('allow_delete', false),
            TextareaField::new('description'),
            SlugField::new('slug')->setTargetFieldName('title')->hideOnIndex(),
            BooleanField::new('isPublished'),
            BooleanField::new('displayInProduct'),
            IntegerField::new('servings')->hideOnIndex(),
            CollectionField::new('ingredients')
                ->setEntryIsComplex(true)
                ->allowAdd()
                ->allowDelete()
                ->setEntryType(RecipeIngredientsType::class)
                ->hideOnIndex(),
            BooleanField::new('isContribution', 'Contribution')->onlyOnDetail(),
            DateField::new('createdAt')->onlyOnDetail(),
            AssociationField::new('user', "Créé par")->setTemplatePath('/admin/recipe_created_by_detail.html.twig')->onlyOnDetail(),
            DateField::new('updatedAt')->onlyOnDetail(),
            AssociationField::new('lastUpdatedBy')->onlyOnDetail(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setDefaultSort([
                    'createdAt' => 'DESC'
                ])
                ->setEntityLabelInPlural('Recettes');
    }

    public function configureActions(Actions $actions): Actions
    {
        $exportSelected = Action::new('exportSelected', 'Exporter')
            ->setIcon('fa fa-download')
            ->linkToCrudAction('exportSelected')
            ->setCssClass('btn');
    
        $exportAll = Action::new('exportAll', 'Tout exporter')
            ->setIcon('fa fa-download')
            ->linkToCrudAction('exportAll')
            ->setCssClass('btn')
            ->createAsGlobalAction();
        
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->addBatchAction($exportSelected)
            ->add(Crud::PAGE_INDEX, $exportAll)
            ->setPermissions([
                ACTION::EDIT => 'ROLE_MANAGER',
                ACTION::DELETE => 'ROLE_MANAGER',
                ACTION::NEW => 'ROLE_MANAGER',
                ACTION::BATCH_DELETE => 'ROLE_MANAGER'
            ])
        ;
    }

    public function exportSelected(
        BatchActionDto $batchActionDto,
        RecipeRepository $recipeRepository
    ) {
        $recipesIDs = $batchActionDto->getEntityIds();
        $recipes = $recipeRepository->findBy(['id' => $recipesIDs]);

        $data = [];
        foreach ($recipes as $recipe) {
            $data[] = $this->csvService->getExportData($recipe);
        }

        return $this->csvService->export($data, 'export_recettes_'.date_create()->format('d-m-y').'.csv');
    }

    public function exportAll(Request $request)
    {
        $context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));
        $filters = $this->filterFactory->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $recipes = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)
            ->getQuery()
            ->getResult();

        $data = [];
        foreach ($recipes as $recipe) {
            $data[] = $this->csvService->getExportData($recipe);
        }

        return $this->csvService->export($data, 'export_recettes_all_'.date_create()->format('d-m-y').'.csv');
    }
}
