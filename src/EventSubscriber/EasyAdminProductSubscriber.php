<?php

namespace App\EventSubscriber;

use App\Entity\Product;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class EasyAdminProductSubscriber implements EventSubscriberInterface
{
    private $security;
    private $manager;

    public function __construct(Security $security, EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->manager = $manager;
    }

    // On écoute l'événement avant que l'entité soit persistée / mise à jour
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setNewProductFields'],
            BeforeEntityUpdatedEvent::class => ['setEditProductFields'],
        ];
    }

    /**
     * Sets slug, createdAt and user fields in database when a new product is created.
     *
     * @return void
     */
    public function setNewProductFields(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Product)) {
            return;
        }

        $createdAt = new DateTime('now');
        $entity->setCreatedAt($createdAt);

        $user = $this->security->getUser();
        $entity->setUser($user);

        $ingredients = $entity->getFoodIngredients();

        foreach ($ingredients as $ingredient) {
            // $ingredient->setProduct($entity);

            $this->manager->persist($ingredient);
        }

        $NutritionalValues = $entity->getNutritionalValues();
        if ($NutritionalValues !== NULL) {
            $energyKJ = $NutritionalValues->getEnergyKJ();
            $energyKcal = round($energyKJ * 0.2388);
            $NutritionalValues->setEnergyKcal($energyKcal);
        }
    }

    /**
     * Sets updatedAt field in database when a product is updated.
     *
     * @return void
     */
    public function setEditProductFields(BeforeEntityUpdatedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Product)) {
            return;
        }

        $updatedAt = new DateTime('now');
        $entity->setUpdatedAt($updatedAt);

        $user = $this->security->getUser();
        $entity->setLastUpdatedBy($user);

        $ingredients = $entity->getFoodIngredients();

        foreach ($ingredients as $ingredient) {
            $this->manager->persist($ingredient);
        }

        $NutritionalValues = $entity->getNutritionalValues();
        if ($NutritionalValues !== NULL) {
            $energyKJ = $NutritionalValues->getEnergyKJ();
            $energyKcal = round($energyKJ * 0.2388);
            $NutritionalValues->setEnergyKcal($energyKcal);
        }
    }
}
