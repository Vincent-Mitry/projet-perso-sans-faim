<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use App\Repository\RatingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @Vich\Uploadable
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainPicture;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="mainPicture")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="product")
     */
    private $images;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dataSheet;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="product_files", fileNameProperty="dataSheet")
     *
     * @var File|null
     */
    private $dataSheetFile;

    /**
     * @ORM\OneToMany(targetEntity=Rating::class, mappedBy="product", orphanRemoval=true)
     */
    private $ratings;

    /**
     * @ORM\OneToMany(targetEntity=Recipe::class, mappedBy="product", orphanRemoval=true)
     */
    private $recipes;

    /**
     * @ORM\OneToOne(targetEntity=NutritionalValues::class, mappedBy="product", cascade={"persist", "remove"})
     */
    private $nutritionalValues;

    /**
     * @ORM\OneToMany(targetEntity=FoodIngredients::class, mappedBy="product", orphanRemoval=true)
     */
    private $foodIngredients;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSeason;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $lastUpdatedBy;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->ratings = new ArrayCollection();
        $this->recipes = new ArrayCollection();
        $this->foodIngredients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMainPicture(): ?string
    {
        return $this->mainPicture;
    }

    public function setMainPicture(string $mainPicture): self
    {
        $this->mainPicture = $mainPicture;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProduct($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getProduct() === $this) {
                $image->setProduct(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDataSheet(): ?string
    {
        return $this->dataSheet;
    }

    public function setDataSheet(string $dataSheet): self
    {
        $this->dataSheet = $dataSheet;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $dataSheetFile
     */
    public function setDataSheetFile(?File $dataSheetFile = null): void
    {
        $this->dataSheetFile = $dataSheetFile;

        if (null !== $dataSheetFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->createdAt = new \DateTime();
        }
    }

    public function getDataSheetFile(): ?File
    {
        return $this->dataSheetFile;
    }

    /**
     * @return Collection|Rating[]
     */
    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function addRating(Rating $rating): self
    {
        if (!$this->ratings->contains($rating)) {
            $this->ratings[] = $rating;
            $rating->setProduct($this);
        }

        return $this;
    }

    public function removeRating(Rating $rating): self
    {
        if ($this->ratings->removeElement($rating)) {
            // set the owning side to null (unless already changed)
            if ($rating->getProduct() === $this) {
                $rating->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Recipe[]
     */
    public function getRecipes(): Collection
    {
        return $this->recipes;
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes[] = $recipe;
            $recipe->setProduct($this);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): self
    {
        if ($this->recipes->removeElement($recipe)) {
            // set the owning side to null (unless already changed)
            if ($recipe->getProduct() === $this) {
                $recipe->setProduct(null);
            }
        }

        return $this;
    }

    public function getNutritionalValues(): ?NutritionalValues
    {
        return $this->nutritionalValues;
    }

    public function setNutritionalValues(NutritionalValues $nutritionalValues): self
    {
        // set the owning side of the relation if necessary
        if ($nutritionalValues->getProduct() !== $this) {
            $nutritionalValues->setProduct($this);
        }

        $this->nutritionalValues = $nutritionalValues;

        return $this;
    }

    /**
     * @return Collection|FoodIngredients[]
     */
    public function getFoodIngredients(): Collection
    {
        return $this->foodIngredients;
    }

    public function addFoodIngredient(FoodIngredients $foodIngredient): self
    {
        if (!$this->foodIngredients->contains($foodIngredient)) {
            $this->foodIngredients[] = $foodIngredient;
            $foodIngredient->setProduct($this);
        }

        return $this;
    }

    public function removeFoodIngredient(FoodIngredients $foodIngredient): self
    {
        if ($this->foodIngredients->removeElement($foodIngredient)) {
            // set the owning side to null (unless already changed)
            if ($foodIngredient->getProduct() === $this) {
                $foodIngredient->setProduct(null);
            }
        }

        return $this;
    }

    public function getIsSeason(): ?bool
    {
        return $this->isSeason;
    }

    public function setIsSeason(bool $isSeason): self
    {
        $this->isSeason = $isSeason;

        return $this;
    }

    public function getLastUpdatedBy(): ?User
    {
        return $this->lastUpdatedBy;
    }

    public function setLastUpdatedBy(?User $lastUpdatedBy): self
    {
        $this->lastUpdatedBy = $lastUpdatedBy;

        return $this;
    }
}
