<?php

namespace App\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

class ChangePassword
{
    /**
     * @SecurityAssert\UserPassword(
     *     message = "Le mot de passe saisi ne correspond pas au mot de passe actuel"
     * )
     */
    protected $oldPassword;

    /**
     * Get message = "Wrong value for your current password"
     */ 
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * Set message = "Wrong value for your current password"
     *
     * @return  self
     */ 
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }
}
