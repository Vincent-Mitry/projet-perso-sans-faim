<?php

namespace App\Entity;

use App\Repository\CommentReplyRepository;
use Doctrine\ORM\Mapping as ORM;

use function PHPUnit\Framework\isEmpty;

/**
 * @ORM\Entity(repositoryClass=CommentReplyRepository::class)
 */
class CommentReply
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=Rating::class, inversedBy="commentReplies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commentParent;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="commentReplies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        if($this != null && !empty($this)) {
            return "oui";
        } else {
            return "non";
        }
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCommentParent(): ?Rating
    {
        return $this->commentParent;
    }

    public function setCommentParent(?Rating $commentParent): self
    {
        $this->commentParent = $commentParent;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
