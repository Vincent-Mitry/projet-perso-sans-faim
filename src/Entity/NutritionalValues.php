<?php

namespace App\Entity;

use App\Repository\NutritionalValuesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NutritionalValuesRepository::class)
 */
class NutritionalValues
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $energyKJ;

    /**
     * @ORM\Column(type="integer")
     */
    private $energyKcal;

    /**
     * @ORM\Column(type="float")
     */
    private $fat;

    /**
     * @ORM\Column(type="float")
     */
    private $fattyAcids;

    /**
     * @ORM\Column(type="float")
     */
    private $carbs;

    /**
     * @ORM\Column(type="float")
     */
    private $sugar;

    /**
     * @ORM\Column(type="float")
     */
    private $protein;

    /**
     * @ORM\Column(type="float")
     */
    private $salt;

    /**
     * @ORM\OneToOne(targetEntity=Product::class, inversedBy="nutritionalValues", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return "Valeurs nutritionnelles";
    }

    public function getEnergyKJ(): ?int
    {
        return $this->energyKJ;
    }

    public function setEnergyKJ(int $energyKJ): self
    {
        $this->energyKJ = $energyKJ;

        return $this;
    }

    public function getEnergyKcal(): ?int
    {
        return $this->energyKcal;
    }

    public function setEnergyKcal(int $energyKcal): self
    {
        $this->energyKcal = $energyKcal;

        return $this;
    }

    public function getFat(): ?float
    {
        return $this->fat;
    }

    public function setFat(float $fat): self
    {
        $this->fat = $fat;

        return $this;
    }

    public function getFattyAcids(): ?float
    {
        return $this->fattyAcids;
    }

    public function setFattyAcids(float $fattyAcids): self
    {
        $this->fattyAcids = $fattyAcids;

        return $this;
    }

    public function getCarbs(): ?float
    {
        return $this->carbs;
    }

    public function setCarbs(float $carbs): self
    {
        $this->carbs = $carbs;

        return $this;
    }

    public function getSugar(): ?float
    {
        return $this->sugar;
    }

    public function setSugar(float $sugar): self
    {
        $this->sugar = $sugar;

        return $this;
    }

    public function getProtein(): ?float
    {
        return $this->protein;
    }

    public function setProtein(float $protein): self
    {
        $this->protein = $protein;

        return $this;
    }

    public function getSalt(): ?float
    {
        return $this->salt;
    }

    public function setSalt(float $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
