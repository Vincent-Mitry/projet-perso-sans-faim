<?php

namespace App\Service;

use App\Entity\CommentReply;
use App\Entity\Rating;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Security;

class CommentReplyService
{
    private $manager;
    private $flashbag;
    private $security;

    public function __construct(
        EntityManagerInterface $manager,
        FlashBagInterface $flashbag,
        Security $security
    ) {
        $this->manager = $manager;
        $this->flashbag = $flashbag;
        $this->security = $security;
    }

    public function persistCommentReply(CommentReply $commentReply, Rating $rating)
    {
        $commentReply->setCreatedAt(new DateTime())
                ->setUser($this->security->getUser())
                ->setCommentParent($rating);
        
        $commentParent = $commentReply->getCommentParent();
        $commentParent->setHasReply(true);

        $this->manager->persist($commentReply);
        $this->manager->flush();

        $this->flashbag->add('success', 'Votre réponse au commentaire vient d\'être publiée');
    }

    public function displayCommentReplyErrorMessage()
    {
        $message = 'Une erreur est survenue lors de la soumission du formulaire de réponse. Veuillez essayer à nouveau.';
        $this->flashbag->add('danger', $message);
    }
}
