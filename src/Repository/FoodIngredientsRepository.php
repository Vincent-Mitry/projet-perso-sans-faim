<?php

namespace App\Repository;

use App\Entity\FoodIngredients;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FoodIngredients|null find($id, $lockMode = null, $lockVersion = null)
 * @method FoodIngredients|null findOneBy(array $criteria, array $orderBy = null)
 * @method FoodIngredients[]    findAll()
 * @method FoodIngredients[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FoodIngredientsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FoodIngredients::class);
    }

    // /**
    //  * @return FoodIngredients[] Returns an array of FoodIngredients objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FoodIngredients
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
