<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rating[]    findAll()
 * @method Rating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rating::class);
    }

    // Récupérer le nombre de notes total d'une année
    // Récupérer le nombre de notes pour chaque mois

    // SELECT COUNT(*), MONTH(`rating`.`created_at`) as month FROM `rating`
    // INNER JOIN `product` ON `rating`.`product_id` = `product`.`id` WHERE YEAR(`rating`.`created_at`) = 2021 
    // GROUP BY month ORDER BY month ASC; 

    /**
     * Finds all comments from object
     */
    public function findAllComments($value)
    {
        if ($value instanceof Product) {
            $object = 'product';
        }

        if ($value instanceof Recipe) {
            $object = 'recipe';
        }

        return $this->createQueryBuilder('r')
                    ->where('r.' . $object . ' IN (:value)')
                    ->andWhere('r.comment IS NOT NULL')
                    ->andWhere('r.isPublished = TRUE')
                    ->orderBy('r.createdAt', 'DESC')
                    ->setParameter('value', $value)
                    ->getQuery()
                    ->getResult();
    }


    /**
     * Finds last three comments to display on page
     */
    public function findLastThreeComments($value)
    {
        if ($value instanceof Product) {
            $object = 'product';
        }

        if ($value instanceof Recipe) {
            $object = 'recipe';
        }

        return $this->createQueryBuilder('r')
                    ->where('r.' . $object . ' IN (:value)')
                    ->andWhere('r.comment IS NOT NULL')
                    ->andWhere('r.isPublished = TRUE')
                    ->orderBy('r.createdAt', 'DESC')
                    ->setMaxResults(3)
                    ->setParameter('value', $value)
                    ->getQuery()
                    ->getResult();
    }

    /**
     * Selects the comments displayed on Homepage
     */
    public function getDisplayHomeComments()
    {
        return $this->createQueryBuilder('r')
                    ->where('r.displayHome = TRUE')
                    ->orderBy('r.createdAt', 'DESC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * Gets average rating and total votes from one object
     */
    public function getAverageRating($value)
    {
        if ($value instanceof Product) {
            $object = 'product';
        }

        if ($value instanceof Recipe) {
            $object = 'recipe';
        }

        return $this->createQueryBuilder('r')
                    ->select('avg(r.rating) as ratingAvg, count(r.rating) as totalRates')
                    ->where('r.' . $object . ' IN (:value)')
                    ->setParameter('value', $value)
                    ->getQuery()
                    ->getResult();
    }

    /**
     * Gets Top 3 items with their average rating and number of ratings
     */
    public function getTopThreeRatedItems($entity)
    {
        if ($entity == Product::class) {
            $entityFK = 'product';
            $field = 'name';
        }

        if ($entity == Recipe::class) {
            $entityFK = 'recipe';
            $field = 'title';
        }
        
        return $this->createQueryBuilder('r')
                ->select('e.' . $field . ', avg(r.rating) as ratingAvg, count(r.rating) as totalRates, e.id')
                ->innerJoin(
                    $entity,
                    'e',
                    Join::WITH,
                    'r.' . $entityFK . '= e.id'
                )
                ->groupBy('e.' . $field)
                ->addGroupBy('e.id')
                ->orderBy('ratingAvg', 'DESC')
                ->addOrderBy('totalRates', 'DESC')
                ->setMaxResults(3)
                ->getQuery()
                ->getResult();
    }

    /**
     * Gets Last 3 items with their average rating and number of ratings
     */
    public function getLastThreeRatedItems($entity)
    {
        if ($entity == Product::class) {
            $entityFK = 'product';
            $field = 'name';
        }

        if ($entity == Recipe::class) {
            $entityFK = 'recipe';
            $field = 'title';
        }
        
        return $this->createQueryBuilder('r')
                ->select('e.' . $field . ', avg(r.rating) as ratingAvg, count(r.rating) as totalRates, e.id')
                ->innerJoin(
                    $entity,
                    'e',
                    'WITH',
                    'r.' . $entityFK . '= e.id'
                )
                ->groupBy('e.' . $field)
                ->addGroupBy('e.id')
                ->orderBy('ratingAvg', 'ASC')
                ->addOrderBy('totalRates', 'DESC')
                ->setMaxResults(3)
                ->getQuery()
                ->getResult();
    }

    // SELECT COUNT(*), MONTH(`rating`.`created_at`) as month FROM `rating`
    // INNER JOIN `product` ON `rating`.`product_id` = `product`.`id` WHERE YEAR(`rating`.`created_at`) = 2021 
    // GROUP BY month ORDER BY month ASC;

    /**
     * Returns an array of associative arrays containing the number of ratings in a month for a given year
     * (doesn't retrieve the data if a month has no ratings)
     *
     * @param [ClassName] $entity
     * @param [int] $year
     * @return array
     */
    public function countAllRatedItemsByMonth($entity, $year)
    {
        if ($entity == Product::class) {
            $entityFK = 'product';
        }

        if ($entity == Recipe::class) {
            $entityFK = 'recipe';
        }

        return $this->createQueryBuilder('r')
                    ->select('MONTH(r.createdAt) as month, COUNT(r) as count')
                    ->innerJoin(
                        $entity,
                        'e',
                        Join::WITH,
                        'r.' . $entityFK . '= e.id'
                    )
                    ->where('YEAR(r.createdAt) = ' . $year)
                    ->groupBy('month')
                    ->orderBy('month', 'ASC')
                    ->getQuery()
                    ->getResult();
    }

    // SELECT COUNT(*), MONTH(`rating`.`created_at`) as month FROM `rating` 
    // INNER JOIN `product` ON `rating`.`product_id` = `product`.`id` 
    // WHERE YEAR(`rating`.`created_at`) = 2021 AND `product`.`id`= 1057
    // GROUP BY month ORDER BY month ASC;

    /**
     * Returns an array of associative arrays containing the number of ratings in a month for a given year
     * (doesn't retrieve the data if a month has no ratings)
     *
     * @param [object] $value
     * @param [int] $year
     * @return void
     */
    public function countRatingsByMonth($value, $year)
    {
        if ($value instanceof Product) {
            $object = 'product';
        }

        if ($value instanceof Recipe) {
            $object = 'recipe';
        }

        return $this->createQueryBuilder('r')
                    ->select('MONTH(r.createdAt) as month, COUNT(r) as count')
                    ->where('YEAR(r.createdAt) = ' . $year)
                    ->andwhere('r.' . $object . ' IN (:value)')
                    ->setParameter('value', $value)
                    ->groupBy('month')
                    ->orderBy('month', 'ASC')
                    ->getQuery()
                    ->getResult();
    }

    /**
     * Gets All items with their average rating and number of ratings
     */
    public function getAllRatedItems($entity)
    {
        if ($entity == Product::class) {
            $entityFK = 'product';
            $field = 'name';
        }

        if ($entity == Recipe::class) {
            $entityFK = 'recipe';
            $field = 'title';
        }

        return $this->createQueryBuilder('r')
                ->select('e.' . $field . ', avg(r.rating) as ratingAvg, count(r.rating) as totalRates, e.id')
                ->innerJoin(
                    $entity,
                    'e',
                    Join::WITH,
                    'r.' . $entityFK . '= e.id'
                )
                ->groupBy('e.' . $field)
                ->addGroupBy('e.id')
                ->orderBy('ratingAvg', 'DESC')
                ->addOrderBy('totalRates', 'DESC')
                ->getQuery()
                ->getResult();
    }

    /**
     * Returns the number of rated items
     */
    public function countRatedItems($entity)
    {
        return count($this->getAllRatedItems($entity));
    }
}
