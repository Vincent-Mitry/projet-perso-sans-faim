<?php

namespace App\Service;

use App\Entity\Recipe;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class RecipeService
{
    private $manager;
    private $flashbag;
    private $slugger;

    public function __construct(
        EntityManagerInterface $manager,
        FlashBagInterface $flashbag,
        SluggerInterface $slugger
    ) {
        $this->manager = $manager;
        $this->flashbag = $flashbag;
        $this->slugger = $slugger;
    }

    public function persistRecipe(Recipe $recipe)
    {
        $recipe->setIsPublished(false)
                ->setDisplayInProduct(false)
                ->setIsContribution(true)
                ->setCreatedAt(new DateTime())
                ->setSlug($this->slugger->slug($recipe->getTitle()));

        $this->manager->persist($recipe);
        $this->manager->flush();

        $message = 'Votre recette a bien été envoyée et est sous attente de validation. Vous serez notifié par mail lors de sa publication';
        $this->flashbag->add('success', $message);
    }

    public function displayRecipeFormErrorMessage()
    {
        $message = 'Une erreur est survenue lors de la soumission de la recette. Veuillez essayer à nouveau.';
        $this->flashbag->add('danger', $message);
    }
}
