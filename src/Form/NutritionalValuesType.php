<?php

namespace App\Form;

use App\Entity\NutritionalValues;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NutritionalValuesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('energyKJ', IntegerType::class)
            ->add('fat', NumberType::class)
            ->add('fattyAcids', NumberType::class)
            ->add('carbs', NumberType::class)
            ->add('sugar', NumberType::class)
            ->add('protein', NumberType::class)
            ->add('salt', NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NutritionalValues::class,
        ]);
    }
}
