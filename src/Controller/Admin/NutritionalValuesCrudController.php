<?php

namespace App\Controller\Admin;

use App\Entity\NutritionalValues;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class NutritionalValuesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NutritionalValues::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            AssociationField::new('product')->setFormTypeOption('disabled', 'disabled'),
            IntegerField::new('energyKJ'),
            IntegerField::new('energyKcal'),
            NumberField::new('fat'),
            NumberField::new('fattyAcids'),
            NumberField::new('carbs'),
            NumberField::new('sugar'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->setPermissions([
                ACTION::EDIT => 'ROLE_ADMIN',
                ACTION::DELETE => 'ROLE_ADMIN',
                ACTION::NEW => 'ROLE_ADMIN',
                ACTION::BATCH_DELETE => 'ROLE_ADMIN'
            ])
        ;
    }
}
