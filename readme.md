# Projet perso Sans Faim

Sans faim est une marque de produits apéritifs en conserve. L'objectif principal du site est de présenter les produits afin que les entreprises intéressées puissent contacter la marque.

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

### Lancer l'environnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```
## Lancer des tests

```bash
php bin/phpunit --testdox
```

## Production

### Envoi des mails de Contact

Les mails de contact sont stockés en BDD, pour les envoyer par mail, il faut mettre en place un cron sur :

```bash
symfony console app:send-contact
```