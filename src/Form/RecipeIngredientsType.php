<?php

namespace App\Form;

use App\Entity\Ingredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeIngredientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                // 'row_attr' => [
                //     'class' => 'col-6'
                // ]
            ])
            ->add('quantity', NumberType::class, [
                'label' => 'Quantité',
                // 'row_attr' => [
                //     'class' => 'col-3'
                // ]
            ])
            ->add('unit', TextType::class, [
                'label' => 'Unité',
                'attr' => [
                    'placeholder' => '(Optionnel)',
                ],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ingredient::class,
        ]);
    }
}
