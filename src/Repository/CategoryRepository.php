<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * Selects the categories to Display
     */
    public function findCategoriesToDisplay()
    {
        return $this->createQueryBuilder('c')
                    ->innerJoin(
                        Product::class,
                        'p',
                        Join::WITH,
                        'c.id = p.category'
                    )
                    ->where('p.status = TRUE')
                    ->groupBy('c.id')
                    ->getQuery()
                    ->getResult();
    }
}
