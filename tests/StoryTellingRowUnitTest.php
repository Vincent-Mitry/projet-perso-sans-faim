<?php

namespace App\Tests;

use App\Entity\StoryTellingRow;
use PHPUnit\Framework\TestCase;

class StoryTellingRowUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $storyTellingRow = new StoryTellingRow();

        $storyTellingRow->setTitle('title')
             ->setContent('content');

        $this->assertTrue('title' === $storyTellingRow->getTitle());
        $this->assertTrue('content' === $storyTellingRow->getContent());
    }

    public function testIsFalse()
    {
        $storyTellingRow = new StoryTellingRow();

        $storyTellingRow->setTitle('title')
                ->setContent('content');

        $this->assertFalse('false' === $storyTellingRow->getTitle());
        $this->assertFalse('false' === $storyTellingRow->getContent());
    }

    public function testIsEmpty()
    {
        $storyTellingRow = new StoryTellingRow();

        $this->assertEmpty($storyTellingRow->getTitle());
        $this->assertEmpty($storyTellingRow->getContent());
    }
}
