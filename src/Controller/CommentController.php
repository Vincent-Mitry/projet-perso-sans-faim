<?php

namespace App\Controller;

use App\Entity\Rating;
use App\Form\RatingType;
use App\Repository\ProductRepository;
use App\Repository\RatingRepository;
use App\Repository\RecipeRepository;
use App\Service\RatingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/commentaires/{entity_param}/{slug}", name="comments_all")
     */
    public function showAllComments(
        $entity_param,
        $slug,
        RatingRepository $ratingRepository,
        ProductRepository $productRepository,
        RecipeRepository $recipeRepository,
        Request $request,
        RatingService $ratingService
    ): Response {

        if ($entity_param == 'produits') {
            $entity = $productRepository->findOneBy(['slug' => $slug]);
        }

        if ($entity_param == 'recettes') {
            $entity = $recipeRepository->findOneBy(['slug' => $slug]);
        }

        $comments = $ratingRepository->findAllComments($entity);
        // Récupération de la moyenne des notes
        $avgRating = $ratingRepository->getAverageRating($entity);

        $referer = $request->headers->get('referer');

        $rating = new Rating();
        $form = $this->createForm(RatingType::class, $rating);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rating = $form->getData();
            $ratingService->persistRating($rating, $entity);

            return $this->redirectToRoute('comments_all', [
                'entity_param' => $entity_param,
                'slug' => $slug,
            ]);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $ratingService->displayRatingErrorMessage();
        }

        return $this->render('comment/comments_all.html.twig', [
            'comments' => $comments,
            'referer' => $referer,
            'entity' => $entity,
            'avgRating' => $avgRating,
            'form' => $form->createView(),
        ]);
    }
}
