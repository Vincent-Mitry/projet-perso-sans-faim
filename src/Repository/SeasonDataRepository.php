<?php

namespace App\Repository;

use App\Entity\SeasonData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SeasonData|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeasonData|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeasonData[]    findAll()
 * @method SeasonData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeasonDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeasonData::class);
    }

    public function findCurrentSeasonData()
    {
        return $this->createQueryBuilder('s')
                    ->where('s.isCurrent = TRUE')
                    ->orderBy('s.id', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    // /**
    //  * @return SeasonData[] Returns an array of SeasonData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SeasonData
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
