// Doc Symfony 
// https://symfony.com/doc/5.4/form/form_collections.html

const addFormToCollection = (e) => {
    const collectionHolder = document.querySelector('.' + e.currentTarget.dataset.collectionHolderClass);

    const item = document.createElement('li');

    // On ajoute du style à la liste des ingrédients
    if (collectionHolder.classList[0] == 'ingredients') {
        item.classList.add('list-unstyled', 'p-3', 'col-md-4', 'col-6');
    }

    // On ajoute du style à la liste des étapes
    if (collectionHolder.classList[0] == 'steps') {
        item.classList.add('col-12', 'list-group-item', 'mb-3', 'border-top-none');
    }

    item.innerHTML = collectionHolder
        .dataset
        .prototype
        .replace(
        /__name__/g,
        collectionHolder.dataset.index
        );

    collectionHolder.appendChild(item);

    collectionHolder.dataset.index++;

    addTagFormDeleteLink(item);
};

const addTagFormDeleteLink = (item) => {
    const removeFormButton = document.createElement('button');
    removeFormButton.innerText = 'Supprimer';
    removeFormButton.classList.add('btn', 'btn-outline-danger', 'btn-sm');
    
    const buttonDiv = document.createElement('div');

    buttonDiv.classList.add('text-end');

    // On ajoute du style au bouton 'supprimer' des étapes
    if (item.parentNode.classList[0] == 'steps') {
        buttonDiv.classList.add('mt-2');
    }

    buttonDiv.append(removeFormButton);
    item.append(buttonDiv);

    removeFormButton.addEventListener('click', (e) => {
        e.preventDefault();
        // remove the li for the tag form
        item.remove();
    });
}

document
    .querySelectorAll('.add_item_link')
    .forEach(btn => {
        btn.addEventListener("click", addFormToCollection)
    });

document
    .querySelectorAll('ul.ingredients li')
    .forEach((tag) => {
        addTagFormDeleteLink(ingredients)
    })
