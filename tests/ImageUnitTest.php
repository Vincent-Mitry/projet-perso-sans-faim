<?php

namespace App\Tests;

use App\Entity\Image;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ImageUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $image = new Image();
        $product = new Product();

        $image->setName('name')
             ->setProduct($product);

        $this->assertTrue('name' === $image->getName());
        $this->assertTrue($product === $image->getProduct());
    }

    public function testIsFalse()
    {
        $image = new Image();
        $product = new Product();

        $image->setName('name')
                ->setProduct($product);

        $this->assertFalse('false' === $image->getName());
        $this->assertFalse(new Product() === $image->getProduct());
    }

    public function testIsEmpty()
    {
        $image = new Image();

        $this->assertEmpty($image->getName());
        $this->assertEmpty($image->getProduct());
    }
}
