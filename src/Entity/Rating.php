<?php

namespace App\Entity;

use App\Repository\RatingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RatingRepository::class)
 */
class Rating
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *      message = "L'email {{ value }} n'est pas valide"
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotBlank
     * @Assert\Range(
     *      min = 1,
     *      max = 5,
     *      notInRangeMessage = "La note doit se situer entre {{ min }} et {{ max }}"
     * )
     */
    private $rating;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\IsTrue(message="Vous devez cocher cette case pour soumettre votre note")
     */
    private $rgpd;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="ratings")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $nickname;

    /**
     * @ORM\ManyToOne(targetEntity=Recipe::class, inversedBy="ratings")
     */
    private $recipe;

    /**
     * @ORM\Column(type="boolean")
     */
    private $displayHome;

    /**
     * @ORM\OneToMany(targetEntity=CommentReply::class, mappedBy="commentParent", orphanRemoval=true)
     */
    private $commentReplies;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasReply;

    public function __construct()
    {
        $this->commentReplies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return 'Note #' . $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getRgpd(): ?bool
    {
        return $this->rgpd;
    }

    public function setRgpd(?bool $rgpd): self
    {
        $this->rgpd = $rgpd;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getDisplayHome(): ?bool
    {
        return $this->displayHome;
    }

    public function setDisplayHome(bool $displayHome): self
    {
        $this->displayHome = $displayHome;

        return $this;
    }

    /**
     * @return Collection|CommentReply[]
     */
    public function getCommentReplies(): Collection
    {
        return $this->commentReplies;
    }

    public function addCommentReply(CommentReply $commentReply): self
    {
        if (!$this->commentReplies->contains($commentReply)) {
            $this->commentReplies[] = $commentReply;
            $commentReply->setCommentParent($this);
        }

        return $this;
    }

    public function removeCommentReply(CommentReply $commentReply): self
    {
        if ($this->commentReplies->removeElement($commentReply)) {
            // set the owning side to null (unless already changed)
            if ($commentReply->getCommentParent() === $this) {
                $commentReply->setCommentParent(null);
            }
        }

        return $this;
    }

    public function getHasReply(): ?bool
    {
        return $this->hasReply;
    }

    public function setHasReply(bool $hasReply): self
    {
        $this->hasReply = $hasReply;

        return $this;
    }
}
