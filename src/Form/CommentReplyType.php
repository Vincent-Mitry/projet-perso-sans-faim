<?php

namespace App\Form;

use App\Entity\CommentReply;
use App\Entity\Rating;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentReplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('comment', TextareaType::class, [
                'label' => 'Réponse'
            ])
            ->add('commentParent', EntityType::class, [
                'label' => 'Commentaire parent',
                'class' => Rating::class,
                'data' => $options['rating'],
                'attr' => [
                    'disabled' => 'disabled',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CommentReply::class,
            'rating' => Rating::class
        ]);
    }
}
