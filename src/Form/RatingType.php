<?php

namespace App\Form;

use App\Entity\Rating;
use App\Form\Type\StarRatingType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RatingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Votre e-mail*',
            ])
            ->add('nickname', TextType::class, [
                'label' => 'Choisissez un pseudo*',
            ])
            ->add('rating', TextType::class, [
                'label' => 'Note sur 5*',
                'required' => true,
                'attr' => [
                    'class' => 'rating',
                    // 'value' => 0,
                ]
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Votre avis',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Optionnel',
                ],
            ])
            ->add('rgpd', CheckboxType::class, [
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rating::class,
        ]);
    }
}
