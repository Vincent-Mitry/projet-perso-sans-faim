<?php

namespace App\Tests;

use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use DateTime;
use PHPUnit\Framework\TestCase;

class RatingUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $rating = new Rating();
        $datetime = new DateTime();
        $product = new Product();
        $recipe = new Recipe();

        $rating->setEmail('true@test.com')
             ->setComment('comment')
             ->setRating(5)
             ->setIsPublished(true)
             ->setRgpd(true)
             ->setCreatedAt($datetime)
             ->setProduct($product)
             ->setNickname('nickname')
             ->setRecipe($recipe);

        $this->assertTrue('true@test.com' === $rating->getEmail());
        $this->assertTrue('comment' === $rating->getComment());
        $this->assertTrue(5 === $rating->getRating());
        $this->assertTrue(true === $rating->getIsPublished());
        $this->assertTrue(true === $rating->getRgpd());
        $this->assertTrue($rating->getCreatedAt() === $datetime);
        $this->assertTrue($rating->getProduct() === $product);
        $this->assertTrue('nickname' === $rating->getNickname());
        $this->assertTrue($rating->getRecipe() === $recipe);
    }

    public function testIsFalse()
    {
        $rating = new Rating();
        $datetime = new DateTime();
        $product = new Product();
        $recipe = new Recipe();

        $rating->setEmail('true@test.com')
             ->setComment('comment')
             ->setRating(5)
             ->setIsPublished(true)
             ->setRgpd(true)
             ->setCreatedAt($datetime)
             ->setProduct($product)
             ->setNickname('nickname')
             ->setRecipe($recipe);

        $this->assertFalse('false@test.com' === $rating->getEmail());
        $this->assertFalse('false' === $rating->getComment());
        $this->assertFalse(1 === $rating->getRating());
        $this->assertFalse(false === $rating->getIsPublished());
        $this->assertFalse(false === $rating->getRgpd());
        $this->assertFalse($rating->getCreatedAt() === new DateTime());
        $this->assertFalse($rating->getProduct() === new Product());
        $this->assertFalse('false' === $rating->getNickname());
        $this->assertFalse($rating->getRecipe() === new Recipe());
    }

    public function testIsEmpty()
    {
        $rating = new Rating();

        $this->assertEmpty($rating->getEmail());
        $this->assertEmpty($rating->getComment());
        $this->assertEmpty($rating->getRating());
        $this->assertEmpty($rating->getIsPublished());
        $this->assertEmpty($rating->getRgpd());
        $this->assertEmpty($rating->getCreatedAt());
        $this->assertEmpty($rating->getProduct());
        $this->assertEmpty($rating->getNickname());
        $this->assertEmpty($rating->getRecipe());
    }
}
