<?php

namespace App\Service;

use App\Entity\Contact;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ContactService
{
    private $manager;
    private $flashbag;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flashbag)
    {
        $this->manager = $manager;
        $this->flashbag = $flashbag;
    }

    public function persistContact(Contact $contact)
    {
        $contact->setIsSent(false)
                ->setCreatedAt(new DateTime());

        $this->manager->persist($contact);
        $this->manager->flush();

        $this->flashbag->add('success', 'Votre message a bien été envoyé, merci.');
    }

    public function displayContactErrorMessage()
    {
        $message = 'Une erreur est survenue lors de la soumission du formulaire. Veuillez essayer à nouveau.';
        $this->flashbag->add('danger', $message);
    }

    public function isSent(Contact $contact)
    {
        $contact->setIsSent(true);
        $this->manager->flush();
    }
}
