<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Recipe;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RecipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Votre email'
            ])
            ->add('nickname', TextType::class, [
                'label' => 'Choisissez un pseudo'
            ])
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'label' => 'Produit de la recette',
                'placeholder' => '-- Sélectionner le produit --',
                'data' => $options['product'],
            ])
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'placeholder' => 'Donnez un titre à votre recette',
                ],
            ])
            ->add('servings', ChoiceType::class, [
                'label' => 'Portions',
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    '10' => 10,
                ],
                'placeholder' => '-- Nombre de personnes --'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'placeholder' => 'Décrivez votre recette en quelques mots',
                ],
            ])
            ->add('ingredients', CollectionType::class, [
                // 'label' => 'Ingrédients',
                'entry_type' => RecipeIngredientsType::class,
                'entry_options' => ['label' => false],
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
            ])
            ->add('steps', CollectionType::class, [
                'label' => 'Etapes de la recette',
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'row_attr' => [
                        'class' => 'col-10'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Ajoutez une photo'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recipe::class,
            'product' => Product::class
        ]);
    }
}
