<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220207184933 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE about_us (id INT AUTO_INCREMENT NOT NULL, story_telling_id INT DEFAULT NULL, video VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_B52303C3D07DA5AC (story_telling_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE story_telling (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE about_us ADD CONSTRAINT FK_B52303C3D07DA5AC FOREIGN KEY (story_telling_id) REFERENCES story_telling (id)');
        $this->addSql('ALTER TABLE user ADD about_us_id INT DEFAULT NULL, ADD avatar VARCHAR(255) DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497CE2CF2D FOREIGN KEY (about_us_id) REFERENCES about_us (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6497CE2CF2D ON user (about_us_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6497CE2CF2D');
        $this->addSql('ALTER TABLE about_us DROP FOREIGN KEY FK_B52303C3D07DA5AC');
        $this->addSql('DROP TABLE about_us');
        $this->addSql('DROP TABLE story_telling');
        $this->addSql('DROP INDEX IDX_8D93D6497CE2CF2D ON user');
        $this->addSql('ALTER TABLE user DROP about_us_id, DROP avatar, DROP description');
    }
}
