<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $category = new Category();
        $product = new Product();

        $category->setName('name')
             ->setDescription('description')
             ->setSlug('slug')
             ->setMainPicture('picture')
             ->addProduct($product)
             ->setShortDescription('shortDescription');

        $this->assertTrue('name' === $category->getName());
        $this->assertTrue('description' === $category->getDescription());
        $this->assertTrue('slug' === $category->getSlug());
        $this->assertTrue('picture' === $category->getMainPicture());
        $this->assertContains($product, $category->getProducts());
        $this->assertTrue('shortDescription' === $category->getShortDescription());
    }

    public function testIsFalse()
    {
        $category = new Category();
        $product = new Product();

        $category->setName('name')
                ->setDescription('description')
                ->setSlug('slug')
                ->setMainPicture('picture')
                ->addProduct($product)
                ->setShortDescription('shortDescription');

        $this->assertFalse('false' === $category->getName());
        $this->assertFalse('false' === $category->getDescription());
        $this->assertFalse('false' === $category->getSlug());
        $this->assertFalse('false' === $category->getMainPicture());
        $this->assertNotContains(new Product(), $category->getProducts());
        $this->assertFalse('false' === $category->getShortDescription());
    }

    public function testIsEmpty()
    {
        $category = new Category();

        $this->assertEmpty($category->getName());
        $this->assertEmpty($category->getDescription());
        $this->assertEmpty($category->getSlug());
        $this->assertEmpty($category->getMainPicture());
        $this->assertEmpty($category->getProducts());
        $this->assertEmpty($category->getShortDescription());
    }
}
