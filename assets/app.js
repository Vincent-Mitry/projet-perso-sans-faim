/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

import bootstrap from 'bootstrap';

var $ = require( "jquery" );
// var Isotope = require('isotope-layout');

// import './vendor/aos/aos';
// import './vendor/isotope-layout/isotope.pkgd';
// import './vendor/swiper/swiper-bundle.min';
// require('./vendor/swiper/swiper-bundle.min copy.css');
import './js/main';

// start the Stimulus application
import './bootstrap';
// import Swiper from 'swiper';

