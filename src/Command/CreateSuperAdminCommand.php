<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\ContactRepository;
use App\Service\ContactService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateSuperAdminCommand extends Command
{
    private $em;
    private $hasher;
    protected static $defaultName = 'app:create-super-admin';

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordHasherInterface $hasher
    ) {
        $this->em = $em;
        $this->hasher = $hasher;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('email', InputArgument::REQUIRED, 'email.')
             ->addArgument('password', InputArgument::REQUIRED, 'password.')
             ->addArgument('firstname', InputArgument::REQUIRED, 'firstname.')
             ->addArgument('lastname', InputArgument::REQUIRED, 'lastname.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = new User();

        $user->setEmail($input->getArgument('email'))
            ->setRoles(['ROLE_SUPER_ADMIN'])
            ->setFirstname($input->getArgument('firstname'))
            ->setLastname($input->getArgument('lastname'))
            ->setDisplayAboutUs(false)
            ->setCreatedAt(new DateTime());

        $password = $this->hasher->hashPassword($user, $input->getArgument('password'));
        $user->setPassword($password);

        $this->em->persist($user);
        $this->em->flush();

        return Command::SUCCESS;
    }
}
