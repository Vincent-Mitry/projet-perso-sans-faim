<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            EmailField::new('email'),
            TextField::new('password')->setFormType(PasswordType::class)->onlyWhenCreating(),
            TextField::new('lastname'),
            TextField::new('firstname'),
            ArrayField::new('roles'),
            TextField::new('phone'),
            ImageField::new('avatar')->setBasePath('/uploads/user/images')->onlyOnIndex(),
            TextField::new('avatarFile')->setFormType(VichImageType::class)->onlyOnForms(),
            TextField::new('position'),
            TextareaField::new('description')->hideOnIndex(),
            BooleanField::new('displayAboutUs')->setPermission('ROLE_SUPER_ADMIN'),
            DateField::new('createdAt')->onlyOnDetail(),
            DateField::new('updatedAt')->onlyOnDetail(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setDefaultSort([
                    'lastname' => 'ASC',
                ])
                ->setEntityLabelInPlural('Users');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->setPermissions([
                ACTION::EDIT => 'ROLE_SUPER_ADMIN',
                ACTION::DELETE => 'ROLE_SUPER_ADMIN',
                ACTION::NEW => 'ROLE_SUPER_ADMIN',
                ACTION::BATCH_DELETE => 'ROLE_SUPER_ADMIN'
            ])
        ;
    }
}
