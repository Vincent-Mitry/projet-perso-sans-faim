<?php

namespace App\Service;

use App\Entity\Recipe;
use Dompdf\Dompdf;
use Dompdf\Options;

class PdfService
{
    private $domPdf;

    public function __construct()
    {
        $this->domPdf = new Dompdf();

        $pdfOptions = new Options();
        $pdfOptions->setIsRemoteEnabled(true);
        $pdfOptions->setIsHtml5ParserEnabled(true);
        $pdfOptions->setChroot(__DIR__);

        $this->domPdf->setOptions($pdfOptions);
    }

    public function showPdfFile($html, $entity)
    {
        $this->domPdf->loadHtml($html);
        $this->domPdf->setPaper('A4', 'portrait');
        $this->domPdf->render();

        if ($entity instanceof Recipe) {
            $filename = 'recette-' . $entity->getSlug();
        }
        $this->domPdf->stream($filename, [
            'Attachment' => false,
        ]);
    }

    // public function showPdfFile($html, $entity)
    // {
    //     $domPdf = new Dompdf();

    //     $pdfOptions = new Options();
    //     $pdfOptions->setIsRemoteEnabled(true);
    //     $pdfOptions->setIsHtml5ParserEnabled(true);
    //     $pdfOptions->setChroot(__DIR__);

    //     $domPdf->setOptions($pdfOptions);

        
    //     $domPdf->loadHtml($html);
    //     $domPdf->setPaper('A4', 'portrait');
    //     $domPdf->render();

    //     if ($entity instanceof Recipe) {
    //         $filename = 'recette-' . $entity->getSlug();
    //     }
    //     $domPdf->stream($filename, [
    //         'Attachment' => false,
    //     ]);
    // }
}
