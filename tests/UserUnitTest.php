<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail('true@test.com')
             ->setFirstname('firstname')
             ->setLastname('lastname')
             ->setPassword('password')
             ->setPhone('06060606')
             ->setAvatar('avatar')
             ->setDescription('description')
             ->setDisplayAboutUs(false);

        $this->assertTrue('true@test.com' === $user->getEmail());
        $this->assertTrue('firstname' === $user->getFirstname());
        $this->assertTrue('lastname' === $user->getLastname());
        $this->assertTrue('password' === $user->getPassword());
        $this->assertTrue('06060606' === $user->getPhone());
        $this->assertTrue('avatar' === $user->getAvatar());
        $this->assertTrue('description' === $user->getDescription());
        $this->assertTrue(false === $user->getDisplayAboutUs());
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('true@test.com')
             ->setFirstname('firstname')
             ->setLastname('lastname')
             ->setPassword('password')
             ->setPhone('06060606')
             ->setAvatar('avatar')
             ->setDescription('description')
             ->setDisplayAboutUs(false);

        $this->assertFalse('false@test.com' === $user->getEmail());
        $this->assertFalse('false' === $user->getFirstname());
        $this->assertFalse('false' === $user->getLastname());
        $this->assertFalse('false' === $user->getPassword());
        $this->assertFalse('false' === $user->getPhone());
        $this->assertFalse('false' === $user->getAvatar());
        $this->assertFalse('false' === $user->getDescription());
        $this->assertFalse(true === $user->getDisplayAboutUs());
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getPhone());
        $this->assertEmpty($user->getAvatar());
        $this->assertEmpty($user->getDescription());
        $this->assertEmpty($user->getDisplayAboutUs());
    }
}
