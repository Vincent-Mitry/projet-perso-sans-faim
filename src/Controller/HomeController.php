<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\RatingRepository;
use App\Repository\RetailersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository,
        RetailersRepository $retailersRepository,
        RatingRepository $ratingRepository
    ): Response {
        return $this->render('home/index.html.twig', [
            'products' => $productRepository->findAllHomepage(),
            'categories' => $categoryRepository->findCategoriesToDisplay(),
            'retailers' => $retailersRepository->findAllHomepage(),
            'comments' => $ratingRepository->getDisplayHomeComments(),
        ]);
    }
}
