<?php

namespace App\Controller\Admin;

use App\Entity\Rating;
use App\Service\CsvService;
use Doctrine\ORM\QueryBuilder;
use App\Repository\RatingRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RatingProductCrudController extends AbstractCrudController
{   
    private $entityRepository;
    private $csvService;
    private $filterFactory;
    
    public function __construct(
        EntityRepository $entityRepository,
        CsvService $csvService,
        FilterFactory $filterFactory
    ) {
        $this->entityRepository = $entityRepository;
        $this->csvService = $csvService;
        $this->filterFactory = $filterFactory;
    }

    public static function getEntityFqcn(): string
    {
        return Rating::class;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->entityRepository->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $response->andWhere('entity.product is not null');

        return $response;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            AssociationField::new('product'),
            NumberField::new('rating'),
            TextareaField::new('comment'),
            EmailField::new('email'),
            DateField::new('createdAt', 'Date de Publication')->hideOnForm(),
            TextField::new('nickname')->onlyOnDetail(),
            BooleanField::new('isPublished')->setPermission('ROLE_ADMIN'),
            BooleanField::new('displayHome', 'Accueil')->setPermission('ROLE_ADMIN'),
            BooleanField::new('hasReply', 'Réponse')->onlyOnIndex()->setTemplatePath('admin/comment_reply_index.html.twig')
                ->onlyOnIndex(),
            CollectionField::new('commentReplies', 'Réponse')->setTemplatePath('admin/comment_reply_detail_product.html.twig')
                ->onlyOnDetail(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setDefaultSort([
                    'createdAt' => 'DESC'
                ])
                ->setEntityLabelInPlural('Ratings')
                ->showEntityActionsInlined();
    }

    public function configureActions(Actions $actions): Actions
    {
        $reply = Action::new('reply', 'Répondre')
            ->linkToRoute('action_reply_comment', function(Rating $entity){
                return [
                    'id' => $entity->getId(),
                ];
            });

        $exportSelected = Action::new('exportSelected', 'Exporter')
            ->setIcon('fa fa-download')
            ->linkToCrudAction('exportSelected')
            ->setCssClass('btn');
        
        $exportAll = Action::new('exportAll', 'Tout exporter')
            ->setIcon('fa fa-download')
            ->linkToCrudAction('exportAll')
            ->setCssClass('btn')
            ->createAsGlobalAction();
        
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_DETAIL, Action::EDIT)
            ->disable(Action::DELETE, Action::NEW)
            ->setPermission(Action::EDIT, 'ROLE_MANAGER')
            ->add(Crud::PAGE_INDEX, $reply)
            ->addBatchAction($exportSelected)
            ->add(Crud::PAGE_INDEX, $exportAll)
            ->setPermission($reply, 'ROLE_MANAGER')
        ;
    }

    /**
     * @Route("/admin/reply_comment", name="action_reply_comment")
     */
    public function replyComment(Request $request) {
        $id = $request->attributes->get('id');

        return $this->redirectToRoute('reply_comment', [
            'id' => $id,
        ]);
    }

    public function exportSelected(
        BatchActionDto $batchActionDto,
        RatingRepository $ratingRepository
    ) {
        $ratingIDs = $batchActionDto->getEntityIds();
        $ratings = $ratingRepository->findBy(['id' => $ratingIDs]);

        $data = [];
        foreach ($ratings as $rating) {
            $data[] = $this->csvService->getExportData($rating);
        }

        return $this->csvService->export($data, 'export_notes_produits_'.date_create()->format('d-m-y').'.csv');
    }

    public function exportAll(Request $request)
    {
        $context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));
        $filters = $this->filterFactory->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $ratings = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)
            ->getQuery()
            ->getResult();

        $data = [];
        foreach ($ratings as $rating) {
            $data[] = $this->csvService->getExportData($rating);
        }

        return $this->csvService->export($data, 'export_notes_produits_all_'.date_create()->format('d-m-y').'.csv');
    }
}
