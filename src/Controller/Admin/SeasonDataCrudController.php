<?php

namespace App\Controller\Admin;

use App\Entity\SeasonData;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SeasonDataCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SeasonData::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            TextField::new('name'),
            TextareaField::new('description')->hideOnIndex(),
            TextareaField::new('shortDescription'),
            SlugField::new('slug')->setTargetFieldName('name')->hideOnIndex(),
            ImageField::new('main_picture')->setBasePath('/uploads/season/images')->onlyOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms()->setFormTypeOption('allow_delete', false),
            BooleanField::new('isCurrent', 'Active'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setDefaultSort([
                    'id' => 'DESC',
                ])
                ->setEntityLabelInPlural('Categories');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->setPermissions([
                ACTION::EDIT => 'ROLE_ADMIN',
                ACTION::DELETE => 'ROLE_ADMIN',
                ACTION::NEW => 'ROLE_ADMIN',
                ACTION::BATCH_DELETE => 'ROLE_ADMIN'
            ])
        ;
    }
}
