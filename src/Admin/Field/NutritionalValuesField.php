<?php

namespace App\Admin\Field;

use App\Form\NutritionalValuesType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

final class NutritionalValuesField implements FieldInterface
{
    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null)
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            // ->setTemplatePath('admin/field/nutritional_values.html.twig')
            ->setFormType(NutritionalValuesType::class)
            ->addCssClass('col-sm-6 col-md-4 col-lg-3')
            ;
    }
}
