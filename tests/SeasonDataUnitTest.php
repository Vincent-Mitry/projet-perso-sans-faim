<?php

namespace App\Tests;

use App\Entity\SeasonData;
use PHPUnit\Framework\TestCase;

class SeasonDataUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $seasonData = new SeasonData();

        $seasonData->setName('name')
             ->setDescription('description')
             ->setSlug('slug')
             ->setMainPicture('picture')
             ->setIsCurrent(true)
             ->setShortDescription('shortDescription');

        $this->assertTrue('name' === $seasonData->getName());
        $this->assertTrue('description' === $seasonData->getDescription());
        $this->assertTrue('slug' === $seasonData->getSlug());
        $this->assertTrue('picture' === $seasonData->getMainPicture());
        $this->assertTrue(true === $seasonData->getIsCurrent());
        $this->assertTrue('shortDescription' === $seasonData->getShortDescription());
    }

    public function testIsFalse()
    {
        $seasonData = new SeasonData();

        $seasonData->setName('name')
                ->setDescription('description')
                ->setSlug('slug')
                ->setMainPicture('picture')
                ->setIsCurrent(true)
                ->setShortDescription('shortDescription');

        $this->assertFalse('false' === $seasonData->getName());
        $this->assertFalse('false' === $seasonData->getDescription());
        $this->assertFalse('false' === $seasonData->getSlug());
        $this->assertFalse('false' === $seasonData->getMainPicture());
        $this->assertFalse(false === $seasonData->getIsCurrent());
        $this->assertFalse('false' === $seasonData->getShortDescription());
    }

    public function testIsEmpty()
    {
        $seasonData = new SeasonData();

        $this->assertEmpty($seasonData->getName());
        $this->assertEmpty($seasonData->getDescription());
        $this->assertEmpty($seasonData->getSlug());
        $this->assertEmpty($seasonData->getMainPicture());
        $this->assertEmpty($seasonData->getIsCurrent());
        $this->assertEmpty($seasonData->getShortDescription());
    }
}
