<?php

namespace App\Command;

use App\Repository\ContactRepository;
use App\Service\ContactService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class SendContactCommand extends Command
{
    private $contactRepository;
    private $mailer;
    private $contactService;
    protected static $defaultName = 'app:send-contact';

    public function __construct(
        ContactRepository $contactRepository,
        MailerInterface $mailer,
        ContactService $contactService
    ) {
        $this->contactRepository = $contactRepository;
        $this->mailer = $mailer;
        $this->contactService = $contactService;
        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // On récupère tous les mails dont le statut est à false (isSent = false)
        $mailsToSend = $this->contactRepository->findBy(['isSent' => false]);

        // On définit l'adresse de l'émetteur et l'adresse du destinataire
        $adressFrom = new Address('33d896cee8b355263468@cloudmailin.net');
        $addressTo = new Address('33d896cee8b355263468@cloudmailin.net');

        // Pour chaque mail à envoyé on crée un composant Email (Mime)
        foreach ($mailsToSend as $mail) {
            $email = new TemplatedEmail();
            $email->from($adressFrom)
                ->to($addressTo)
                ->replyTo($mail->getEmail())
                ->subject('Formulaire de contact : ' . $mail->getSubject())
                // ->text($mail->getMessage())
                ->htmlTemplate('email/contact_mail.html.twig')
                ->context([
                    'contact' => $mail,
                ]);

            // On envoie les mails avec le composant mailer
            $this->mailer->send($email);

            // On met à jour le statut isSent = true
            $this->contactService->isSent($mail);
        }

        return Command::SUCCESS;
    }
}
