<?php

namespace App\Service;

use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class RatingService
{
    private $manager;
    private $flashbag;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flashbag)
    {
        $this->manager = $manager;
        $this->flashbag = $flashbag;
    }

    public function persistRating(Rating $rating, $entity)
    {
        if ($entity instanceof Product) {
            $rating->setProduct($entity)
                    ->setRecipe(null);
        }

        if ($entity instanceof Recipe) {
            $rating->setProduct(null)
                    ->setRecipe($entity);
        }
        
        $rating->setIsPublished(true)
                ->setCreatedAt(new DateTime())
                ->setDisplayHome(false)
                ->setHasReply(false);

        $this->manager->persist($rating);
        $this->manager->flush();

        $this->flashbag->add('success', 'Votre note a bien été prise en compte.');
    }

    public function displayRatingErrorMessage()
    {
        $message = 'Une erreur est survenue lors de la soumission de la note. Veuillez essayer à nouveau.';
        $this->flashbag->add('danger', $message);
    }

    /**
     * Returns an array with the number of rated items (value) for each month (key)
     *
     * @param [array] $array
     * @return array
     */
    public function createCountPerMonthArrayFromQuery($array)
    {
        // On initialise notre tableau de données
        // avec les mois (clé) et le nombre de votes (valeur)
        $list = [
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0,
        ];

        foreach ($array as $row) {
            $list[$row['month']] = $row['count'];
        }

        return $list;
    }
}
