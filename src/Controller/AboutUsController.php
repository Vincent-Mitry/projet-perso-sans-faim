<?php

namespace App\Controller;

use App\Repository\StoryTellingRowRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends AbstractController
{
    /**
     * @Route("/qui-sommes-nous", name="about_us")
     */
    public function showAboutUs(
        UserRepository $userRepository,
        StoryTellingRowRepository $storyTellingRowRepository
    ): Response {
        return $this->render('about_us/about_us.html.twig', [
            'teamMembers' => $userRepository->getUsersAboutUs(),
            'storyTelling' => $storyTellingRowRepository->findAll(),
        ]);
    }
}
