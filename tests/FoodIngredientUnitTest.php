<?php

namespace App\Tests;

use App\Entity\FoodIngredients;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class FoodIngredientUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $foodIngredient = new FoodIngredients();
        $product = new Product();

        $foodIngredient->setName('name')
             ->setPercentage(10.1)
             ->setIsAllergen(true)
             ->setProduct($product);

        $this->assertTrue('name' === $foodIngredient->getName());
        $this->assertTrue(10.1 === $foodIngredient->getPercentage());
        $this->assertTrue(true === $foodIngredient->getIsAllergen());
        $this->assertTrue($product === $foodIngredient->getProduct());
    }

    public function testIsFalse()
    {
        $foodIngredient = new FoodIngredients();
        $product = new Product();

        $foodIngredient->setName('name')
                ->setPercentage(10.1)
                ->setIsAllergen(true)
                ->setProduct($product);

        $this->assertFalse('false' === $foodIngredient->getName());
        $this->assertFalse(20.2 === $foodIngredient->getPercentage());
        $this->assertFalse(false === $foodIngredient->getIsAllergen());
        $this->assertFalse(new Product() === $foodIngredient->getProduct());
    }

    public function testIsEmpty()
    {
        $foodIngredient = new FoodIngredients();

        $this->assertEmpty($foodIngredient->getName());
        $this->assertEmpty($foodIngredient->getPercentage());
        $this->assertEmpty($foodIngredient->getIsAllergen());
        $this->assertEmpty($foodIngredient->getProduct());
    }
}
