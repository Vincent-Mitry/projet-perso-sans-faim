<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GpsCoordinatesRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=GpsCoordinatesRepository::class)
 */
class GpsCoordinates
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("retailer:read")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8)
     * @Groups("retailer:read")
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8)
     * @Groups("retailer:read")
     */
    private $longitude;

    /**
     * @ORM\OneToOne(targetEntity=Retailers::class, inversedBy="gpsCoordinates", cascade={"persist", "remove"})
     */
    private $retailer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return "Coordonnées GPS";
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getRetailer(): ?Retailers
    {
        return $this->retailer;
    }

    public function setRetailer(?Retailers $retailer): self
    {
        $this->retailer = $retailer;

        return $this;
    }
}
