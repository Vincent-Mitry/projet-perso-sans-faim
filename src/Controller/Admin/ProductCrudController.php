<?php

namespace App\Controller\Admin;

use App\Admin\Field\NutritionalValuesField;
use App\Entity\Product;
use App\Form\FoodIngredientsType;
use App\Repository\ProductRepository;
use App\Service\CsvService;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductCrudController extends AbstractCrudController
{
    private $csvService;
    private $filterFactory;
    
    public function __construct(
        CsvService $csvService,
        FilterFactory $filterFactory
    ) {
        $this->csvService = $csvService;
        $this->filterFactory = $filterFactory;
    }
    
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            TextField::new('name'),
            AssociationField::new('category'),
            ImageField::new('main_picture')->setBasePath('/uploads/product/images')->onlyOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms()->setFormTypeOption('allow_delete', false),
            TextareaField::new('description'),
            BooleanField::new('status'),
            BooleanField::new('isSeason'),
            SlugField::new('slug')->setTargetFieldName('name')->hideOnIndex(),
            TextField::new('dataSheetFile')->setTemplatePath('admin/data_sheet.html.twig')->onlyOnDetail(),
            TextField::new('dataSheetFile')->setFormType(VichFileType::class)->onlyOnForms(),
            CollectionField::new('foodIngredients', 'Liste des ingrédients')->setEntryIsComplex(true)
                                                    ->allowAdd()
                                                    ->allowDelete()
                                                    ->setEntryType(FoodIngredientsType::class)
                                                    ->hideOnIndex(),
            AssociationField::new('nutritionalValues')->setCrudController(NutritionalValuesCrudController::class)->onlyOnDetail(),
            NutritionalValuesField::new('nutritionalValues')->onlyOnForms(),
            DateField::new('createdAt')->onlyOnDetail(),
            AssociationField::new('user', "Créé par")->onlyOnDetail(),
            DateField::new('updatedAt')->onlyOnDetail(),
            AssociationField::new('lastUpdatedBy')->onlyOnDetail(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setDefaultSort([
                    'status' => 'DESC',
                    'createdAt' => 'DESC'
                ])
                ->setEntityLabelInPlural('Products');
    }

    public function configureActions(Actions $actions): Actions
    {
        $exportSelected = Action::new('exportSelected', 'Exporter')
            ->setIcon('fa fa-download')
            ->linkToCrudAction('exportSelected')
            ->setCssClass('btn');
        
        $exportAll = Action::new('exportAll', 'Tout exporter')
            ->setIcon('fa fa-download')
            ->linkToCrudAction('exportAll')
            ->setCssClass('btn')
            ->createAsGlobalAction();
        
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->addBatchAction($exportSelected)
            ->add(Crud::PAGE_INDEX, $exportAll)
            ->setPermissions([
                ACTION::EDIT => 'ROLE_ADMIN',
                ACTION::DELETE => 'ROLE_ADMIN',
                ACTION::NEW => 'ROLE_ADMIN',
                ACTION::BATCH_DELETE => 'ROLE_ADMIN'
            ])

        ;
    }

    public function exportSelected(
        BatchActionDto $batchActionDto,
        ProductRepository $productRepository
    ) {
        $productIDs = $batchActionDto->getEntityIds();
        $products = $productRepository->findBy(['id' => $productIDs]);

        $data = [];
        foreach ($products as $product) {
            $data[] = $this->csvService->getExportData($product);
        }

        return $this->csvService->export($data, 'export_produits_'.date_create()->format('d-m-y').'.csv');
    }

    public function exportAll(Request $request)
    {
        $context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));
        $filters = $this->filterFactory->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $products = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)
            ->getQuery()
            ->getResult();

        $data = [];
        foreach ($products as $product) {
            $data[] = $this->csvService->getExportData($product);
        }

        return $this->csvService->export($data, 'export_produits_all_'.date_create()->format('d-m-y').'.csv');
    }
}
