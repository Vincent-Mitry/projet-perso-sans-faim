<?php

namespace App\EventSubscriber;

use App\Entity\Retailers;
use DateTime;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminRetailerSubscriber implements EventSubscriberInterface
{
    // On écoute l'événement avant que l'entité soit persistée / mise à jour
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setNewRetailerFields'],
            BeforeEntityUpdatedEvent::class => ['setEditRetailerFields'],
        ];
    }

    /**
     * Sets slug, createdAt and user fields in database when a new product is created.
     *
     * @return void
     */
    public function setNewRetailerFields(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Retailers)) {
            return;
        }

        $createdAt = new DateTime('now');
        $entity->setCreatedAt($createdAt);
    }

    /**
     * Sets updatedAt field in database when a product is updated.
     *
     * @return void
     */
    public function setEditRetailerFields(BeforeEntityUpdatedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Retailers)) {
            return;
        }

        $updatedAt = new DateTime('now');
        $entity->setUpdatedAt($updatedAt);
    }
}
