<?php

namespace App\Controller\Admin;

use App\Admin\Field\GpsCoordinatesField;
use App\Entity\Retailers;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RetailersCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Retailers::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            TextField::new('name'),
            TextField::new('website'),
            ImageField::new('logoImage')->setBasePath('/uploads/retailers/images')->onlyOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms()->setFormTypeOption('allow_delete', false),
            TextField::new('phone'),
            TextField::new('address'),
            GpsCoordinatesField::new('gpsCoordinates')->onlyOnForms(),
            TextField::new('GpsCoordinates')->onlyOnDetail()->setTemplatePath('/admin/gps_coordinates_detail.html.twig'),
            DateField::new('createdAt')->onlyOnDetail(),
            DateField::new('updatedAt')->onlyOnDetail(),
            BooleanField::new('displayHome'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setDefaultSort([
                    'name' => 'ASC',
                ])
                ->setEntityLabelInPlural('Retailers');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->setPermissions([
                ACTION::EDIT => 'ROLE_ADMIN',
                ACTION::DELETE => 'ROLE_ADMIN',
                ACTION::NEW => 'ROLE_ADMIN',
                ACTION::BATCH_DELETE => 'ROLE_ADMIN'
            ])
        ;
    }
}
