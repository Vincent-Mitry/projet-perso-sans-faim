<?php

namespace App\Form;

use App\Entity\Product;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RatingsByProductChartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $currentYear = (new DateTime('now'))->format('Y');

        $builder
            ->add('product', EntityType::class, [
                'placeholder' => '-- Sélectionnez un produit --',
                'class' => Product::class,
            ])
            ->add('year', ChoiceType::class, [
                'placeholder' => '-- Selectionnez une année --',
                'choices' => array_combine(
                    range(2020, $currentYear),
                    range(2020, $currentYear)
                ),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([

        ]);
    }
}
