<?php

namespace App\Admin\Field;

use App\Form\GpsCoordinatesType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

final class GpsCoordinatesField implements FieldInterface
{
    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null)
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            // ->setTemplatePath('admin/field/nutritional_values.html.twig')
            ->setFormType(GpsCoordinatesType::class)
            ->addCssClass('col-sm-4 col-md-3 col-lg-2')
            ;
    }
}
