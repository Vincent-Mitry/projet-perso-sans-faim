<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\RatingRepository;
use App\Service\RatingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiRatingStatsController extends AbstractController
{
    /**
     * @Route("/api/rating-stats/product/{id}/{year}", name="api_product_rating_stats", methods={"GET"}, options={"expose"=true}, priority=-1)
     */
    public function sendProductRatingStatsByYear(
        $id,
        $year,
        RatingRepository $ratingRepository,
        ProductRepository $productRepository,
        RatingService $ratingService
    ): Response {
        $product = $productRepository->findOneBy(["id" => $id]);
        $query = $ratingRepository->countRatingsByMonth($product, $year);

        return $this->json($ratingService->createCountPerMonthArrayFromQuery($query), 200);
    }

    /**
     * @Route("/api/rating-stats/product/all/{year}", name="api_all_products_rating_stats", methods={"GET"}, options={"expose"=true})
     */
    public function sendAllProductsRatingStatsByYear(
        $year,
        RatingRepository $ratingRepository,
        RatingService $ratingService): Response
    {
        $query = $ratingRepository->countAllRatedItemsByMonth(Product::class, $year);

        return $this->json($ratingService->createCountPerMonthArrayFromQuery($query), 200);
    }
}
