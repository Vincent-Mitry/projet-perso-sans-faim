<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use App\Form\RatingType;
use App\Form\RecipeType;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\RatingRepository;
use App\Repository\RecipeRepository;
use App\Service\PdfService;
use App\Service\RatingService;
use App\Service\RecipeService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecipeController extends AbstractController
{
    /**
     * @Route("/recettes", name="recipes_all")
     */
    public function showAllRecipes(
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository
    ): Response {
        return $this->render('recipe/recipes_all.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'products' => $productRepository->findAllHomepage(),
        ]);
    }

    /**
     * @Route("/recettes/{product_slug}", name="recipes_by_product", priority=-1)
     * @ParamConverter("product", options={"mapping": {"product_slug": "slug"}})
     */
    public function showRecipesByProduct(Product $product, RecipeRepository $recipeRepository)
    {
        return $this->render('recipe/recipes_by_product.html.twig', [
            'product' => $product,
            'recipes' => $recipeRepository->findPublishedRecipesByProduct($product),
        ]);
    }

    /**
     * @Route("/recettes/{product_slug}/{recipe_slug}", name="recipe_single", priority=-1)
     * @ParamConverter("product", options={"mapping": {"product_slug": "slug"}})
     * @ParamConverter("recipe", options={"mapping": {"recipe_slug": "slug"}})
     */
    public function showRecipe(
        Recipe $recipe,
        Request $request,
        RatingService $ratingService,
        RatingRepository $ratingRepository
    ): Response {
        // Formulaire de notation
        $rating = new Rating();
        $form = $this->createForm(RatingType::class, $rating);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rating = $form->getData();
            $ratingService->persistRating($rating, $recipe);

            return $this->redirectToRoute('recipe_single', [
                'product_slug' => $recipe->getProduct()->getSlug(),
                'recipe_slug' => $recipe->getSlug(),
            ]);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $ratingService->displayRatingErrorMessage();
        }

        // Récupération de la moyenne des notes
        $avgRating = $ratingRepository->getAverageRating($recipe);

        // Récupération des 3 derniers commentaires
        $lastThreeComments = $ratingRepository->findLastThreeComments($recipe);

        return $this->render('recipe/recipe_single.html.twig', [
            'recipe' => $recipe,
            'form' => $form->createView(),
            'lastThreeComments' => $lastThreeComments,
            'avgRating' => $avgRating,
        ]);
    }

    /**
     * @Route("/pdf/recette/{slug}", name="recipe_pdf")
     */
    public function generateRecipePdf(Recipe $recipe, PdfService $pdfService)
    {
        $html = $this->renderView('recipe/pdf_recipe_single.html.twig', [
            'recipe' => $recipe,
        ]);
        $pdfService->showPdfFile($html, $recipe);
    }

    /**
     * @Route("/recettes/proposer-une-recette/{product_id?}", name="recipe_create_form")
     */
    public function createRecipe(
        $product_id ="",
        Request $request,
        RecipeService $recipeService,
        ProductRepository $productRepository
    ) {
        $product = $productRepository->findOneBy(['id' => $product_id]);
        // Formulaire de création de recette
        $recipe = new Recipe();
        $form = $this->createForm(RecipeType::class, $recipe, [
            'product' => $product
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();
            $recipeService->persistRecipe($recipe);

            return $this->redirectToRoute('recipes_all');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $recipeService->displayRecipeFormErrorMessage();
        }

        return $this->render('recipe/recipe_create_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
