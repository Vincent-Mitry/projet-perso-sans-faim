<?php

namespace App\Tests;

use App\Entity\Ingredient;
use App\Entity\Product;
use App\Entity\Recipe;
use DateTime;
use PHPUnit\Framework\TestCase;

class RecipeUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $recipes = new Recipe();
        $datetime = new DateTime();
        $ingredient = new Ingredient();
        $steps = ['step'];
        $product = new Product();

        $recipes->setTitle('title')
             ->setMainPicture('picture')
             ->addIngredient($ingredient)
             ->setSteps($steps)
             ->setDescription('description')
             ->setCreatedAt($datetime)
             ->setUpdatedAt($datetime)
             ->setSlug('slug')
             ->setIsPublished(true)
             ->setDisplayInProduct(true)
             ->setProduct($product)
             ->setServings(1);

        $this->assertTrue('title' === $recipes->getTitle());
        $this->assertTrue('picture' === $recipes->getMainPicture());
        $this->assertContains($ingredient, $recipes->getIngredients());
        $this->assertTrue($recipes->getSteps() === $steps);
        $this->assertTrue('description' === $recipes->getDescription());
        $this->assertTrue($datetime === $recipes->getCreatedAt());
        $this->assertTrue($datetime === $recipes->getUpdatedAt());
        $this->assertTrue('slug' === $recipes->getSlug());
        $this->assertTrue(true === $recipes->getIsPublished());
        $this->assertTrue(true === $recipes->getDisplayInProduct());
        $this->assertTrue($recipes->getProduct() === $product);
        $this->assertTrue(1 === $recipes->getServings());
    }

    public function testIsFalse()
    {
        $recipes = new Recipe();
        $datetime = new DateTime();
        $ingredient = new Ingredient();
        $steps = ['step'];
        $falseArray = ['false'];
        $product = new Product();

        $recipes->setTitle('title')
             ->setMainPicture('picture')
             ->addIngredient($ingredient)
             ->setSteps($steps)
             ->setDescription('description')
             ->setCreatedAt($datetime)
             ->setUpdatedAt($datetime)
             ->setSlug('slug')
             ->setIsPublished(true)
             ->setDisplayInProduct(true)
             ->setProduct($product)
             ->setServings(1);

        $this->assertFalse('false' === $recipes->getTitle());
        $this->assertFalse('false' === $recipes->getMainPicture());
        $this->assertNotContains(new Ingredient(), $recipes->getIngredients());
        $this->assertFalse($recipes->getSteps() === $falseArray);
        $this->assertFalse('false' === $recipes->getDescription());
        $this->assertFalse(new DateTime() === $recipes->getCreatedAt());
        $this->assertFalse(new DateTime() === $recipes->getUpdatedAt());
        $this->assertFalse(false === $recipes->getSlug());
        $this->assertFalse(false === $recipes->getIsPublished());
        $this->assertFalse(false === $recipes->getDisplayInProduct());
        $this->assertFalse($recipes->getProduct() === new Product());
        $this->assertFalse(2 === $recipes->getServings());
    }

    public function testIsEmpty()
    {
        $recipes = new Recipe();

        $this->assertEmpty($recipes->getTitle());
        $this->assertEmpty($recipes->getMainPicture());
        $this->assertEmpty($recipes->getIngredients());
        $this->assertEmpty($recipes->getSteps());
        $this->assertEmpty($recipes->getDescription());
        $this->assertEmpty($recipes->getCreatedAt());
        $this->assertEmpty($recipes->getUpdatedAt());
        $this->assertEmpty($recipes->getSlug());
        $this->assertEmpty($recipes->getIsPublished());
        $this->assertEmpty($recipes->getDisplayInProduct());
        $this->assertEmpty($recipes->getProduct());
        $this->assertEmpty($recipes->getServings());
    }
}
