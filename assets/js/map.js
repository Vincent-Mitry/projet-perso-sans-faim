import '../scss/map.scss';

import L from 'leaflet';

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

// Importation des routes FOS JS Bundle
const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

Routing.setRoutingData(routes);

// ----------------------------------------------------------------------------------------------------------------------- //
// -----------------------------------   Affichage de la carte   --------------------------------------------------------- // 
// ----------------------------------------------------------------------------------------------------------------------- // 

// On initialise la carte
var map = L.map("map").setView([45.5, 2], 5);

// On charge les tuiles
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

// L.marker([51.5, -0.09])
//   .addTo(map)
//   .bindPopup("A pretty CSS3 popup.<br> Easily customizable.")
//   .openPopup();


// ----------------------------------------------------------------------------------------------------------------------- //
// -----------------------------------   Affichage des Markers sur la carte   -------------------------------------------- // 
// ----------------------------------------------------------------------------------------------------------------------- // 

/**
 * Affiche les markers sur la carte grâce aux coordonnées GPS
 */
function displayMarkersInMap(data) {

	console.log(data);

	data.forEach(retailer => {
		let lat = retailer.gpsCoordinates.latitude;
    	let lon = retailer.gpsCoordinates.longitude;
    	L.marker([lat, lon])
      		.addTo(map)
      		.bindPopup(
				`<h3 class="h4 mb-1">${retailer.name}</h3>
				<p class="my-0">${retailer.address}</p>
				<p class="my-0">${retailer.phone}</p>
				<a href="${Routing.generate('retailer_page', { id: retailer.id })}" target="_blank">Page Web</a>`
			  );
	});
}

/**
 * Méthode gérant zoom sur un partenaire sur la carte et affiche ses données
 * @param {*} retailerData 
 */
function showSelectedRetailerOnMap(retailerData) {
	const lat = retailerData.gpsCoordinates.latitude;
	const lon = retailerData.gpsCoordinates.longitude;
	L.marker([lat,lon])
		.addTo(map)
		.bindPopup(
			`<h3 class="h4 mb-1">${retailerData.name}</h3>
				<p class="my-0">${retailerData.address}</p>
				<p class="my-0">${retailerData.phone}</p>
				<a href="${Routing.generate('retailer_page', { id: retailerData.id })}" target="_blank">Page Web</a>`
		)
		.openPopup();

	map.setView([lat,lon], 12);
}

/**
 * Méthode qui permet de changer le style du partenaire sélectionné
 * @param {*} retailerId 
 */
function changeSelectedRetailerStyle(retailerId) {
	const retailerElementList = document.querySelectorAll('.selectRetailer');
	const retailerElement = document.getElementById(retailerId);
	
	retailerElementList.forEach(retailerElement => {
		retailerElement.classList.remove('fw-bold', 'text-dark')
	});
	retailerElement.classList.add('fw-bold', 'text-dark');
}

/**
 * Méthode qui permet de récupérer les données des partenaires au format Json
 */
async function fetchRetailersData() {
  	// const url = 'https://127.0.0.1:8000/api/retailers';
	const url = `${Routing.generate('api_retailers_all')}`;
  
	const retailersData = await fetch(url)
  	.then(res => res.json())
  	.then(Json => Json);

  displayMarkersInMap(retailersData);
}

const retailerElements = document.querySelectorAll('.selectRetailer');
retailerElements.forEach(retailerElement => {
	retailerElement.addEventListener('click', handleSelectedRetailer);
})

/**
 * Méthode gérant le click sur un partenaire
 */
async function handleSelectedRetailer(e) {
	const retailerId = e.target.id;
	
	// const url = `https://127.0.0.1:8000/api/retailers/${retailerId}`;
	const url = `${Routing.generate('api_retailers_byId', { id: retailerId })}`;

	const retailerData = await fetch(url)
	.then(res => res.json())
	.then(Json => Json);

	showSelectedRetailerOnMap(retailerData);
	changeSelectedRetailerStyle(retailerId);
}

fetchRetailersData();
