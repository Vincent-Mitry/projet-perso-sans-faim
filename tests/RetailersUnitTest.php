<?php

namespace App\Tests;

use App\Entity\Retailers;
use DateTime;
use PHPUnit\Framework\TestCase;

class RetailersUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $retailers = new Retailers();
        $datetime = new DateTime();

        $retailers->setName('name')
             ->setWebsite('website')
             ->setLogoImage('logo')
             ->setAddress('address')
             ->setPhone('06060606')
             ->setCreatedAt($datetime)
             ->setUpdatedAt($datetime)
             ->setDisplayHome(true);

        $this->assertTrue('name' === $retailers->getName());
        $this->assertTrue('website' === $retailers->getWebsite());
        $this->assertTrue('logo' === $retailers->getLogoImage());
        $this->assertTrue('address' === $retailers->getAddress());
        $this->assertTrue('06060606' === $retailers->getPhone());
        $this->assertTrue($datetime === $retailers->getCreatedAt());
        $this->assertTrue($datetime === $retailers->getUpdatedAt());
        $this->assertTrue(true === $retailers->getDisplayHome());
    }

    public function testIsFalse()
    {
        $retailers = new Retailers();
        $datetime = new DateTime();

        $retailers->setName('name')
             ->setWebsite('website')
             ->setLogoImage('logo')
             ->setAddress('address')
             ->setPhone('06060606')
             ->setCreatedAt($datetime)
             ->setUpdatedAt($datetime)
             ->setDisplayHome(true);

        $this->assertFalse('false' === $retailers->getName());
        $this->assertFalse('false' === $retailers->getWebsite());
        $this->assertFalse('false' === $retailers->getLogoImage());
        $this->assertFalse('false' === $retailers->getAddress());
        $this->assertFalse('false' === $retailers->getPhone());
        $this->assertFalse(new DateTime() === $retailers->getCreatedAt());
        $this->assertFalse(new DateTime() === $retailers->getUpdatedAt());
        $this->assertFalse(false === $retailers->getDisplayHome());
    }

    public function testIsEmpty()
    {
        $retailers = new Retailers();

        $this->assertEmpty($retailers->getName());
        $this->assertEmpty($retailers->getWebsite());
        $this->assertEmpty($retailers->getLogoImage());
        $this->assertEmpty($retailers->getAddress());
        $this->assertEmpty($retailers->getPhone());
        $this->assertEmpty($retailers->getCreatedAt());
        $this->assertEmpty($retailers->getUpdatedAt());
        $this->assertEmpty($retailers->getDisplayHome());
    }
}
