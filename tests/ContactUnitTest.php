<?php

namespace App\Tests;

use App\Entity\Contact;
use DateTime;
use PHPUnit\Framework\TestCase;

class ContactUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $contact = new Contact();
        $datetime = new DateTime();

        $contact->setLastname('lastname')
             ->setFirstname('firstname')
             ->setCompany('company')
             ->setActivity('activity')
             ->setPosition('position')
             ->setEmail('mail@example.com')
             ->setPhone('0123456789')
             ->setSubject('subject')
             ->setMessage('message')
             ->setCreatedAt($datetime)
             ->setIsSent(false)
             ->setRgpd(true);

        $this->assertTrue('lastname' === $contact->getLastname());
        $this->assertTrue('firstname' === $contact->getFirstname());
        $this->assertTrue('company' === $contact->getCompany());
        $this->assertTrue('activity' === $contact->getActivity());
        $this->assertTrue('position' === $contact->getPosition());
        $this->assertTrue('mail@example.com' === $contact->getEmail());
        $this->assertTrue('0123456789' === $contact->getPhone());
        $this->assertTrue('subject' === $contact->getSubject());
        $this->assertTrue('message' === $contact->getMessage());
        $this->assertTrue($datetime === $contact->getCreatedAt());
        $this->assertTrue(false === $contact->getIsSent());
        $this->assertTrue(true === $contact->getRgpd());
    }

    public function testIsFalse()
    {
        $contact = new Contact();
        $datetime = new DateTime();

        $contact->setLastname('lastname')
                ->setFirstname('firstname')
                ->setCompany('company')
                ->setActivity('activity')
                ->setPosition('position')
                ->setEmail('mail@example.com')
                ->setPhone('0123456789')
                ->setSubject('subject')
                ->setMessage('message')
                ->setCreatedAt($datetime)
                ->setIsSent(false)
                ->setRgpd(true);

        $this->assertFalse('false' === $contact->getLastname());
        $this->assertFalse('false' === $contact->getFirstname());
        $this->assertFalse('false' === $contact->getCompany());
        $this->assertFalse('false' === $contact->getActivity());
        $this->assertFalse('false' === $contact->getPosition());
        $this->assertFalse('false' === $contact->getEmail());
        $this->assertFalse('0987654321' === $contact->getPhone());
        $this->assertFalse('false' === $contact->getSubject());
        $this->assertFalse('false' === $contact->getMessage());
        $this->assertFalse(new DateTime() === $contact->getCreatedAt());
        $this->assertFalse(true === $contact->getIsSent());
        $this->assertFalse(false === $contact->getRgpd());
    }

    public function testIsEmpty()
    {
        $contact = new Contact();

        $this->assertEmpty($contact->getLastname());
        $this->assertEmpty($contact->getFirstname());
        $this->assertEmpty($contact->getCompany());
        $this->assertEmpty($contact->getActivity());
        $this->assertEmpty($contact->getPosition());
        $this->assertEmpty($contact->getEmail());
        $this->assertEmpty($contact->getPhone());
        $this->assertEmpty($contact->getSubject());
        $this->assertEmpty($contact->getMessage());
        $this->assertEmpty($contact->getCreatedAt());
        $this->assertEmpty($contact->getIsSent());
        $this->assertEmpty($contact->getRgpd());
    }
}
