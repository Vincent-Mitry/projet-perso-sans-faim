<?php

namespace App\Service;

use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use App\Repository\RatingRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CsvService
{
    private $ratingRepository;

    public function __construct(RatingRepository $ratingRepository)
    {
        $this->ratingRepository = $ratingRepository;
    }
    
    public function export($data, $filename)
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        $response = new Response($serializer->encode($data, CsvEncoder::FORMAT));
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', "attachment; filename=\"$filename\"");
        return $response;
    }

    public function import($filename, $options = [])
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        return $serializer->decode(file_get_contents($filename), CsvEncoder::FORMAT, $options);
    }

    public function getExportData($entity)
    {
        if ($entity instanceof Product) {
            return [
                'id' => $entity->getId(),
                'nom' => $entity->getName(),
                'catégorie' => $entity->getCategory(),
                'saison' => $entity->getIsSeason() ? 'oui' : 'non',
                'note' => $this->ratingRepository->getAverageRating($entity),
                'date_création' => $entity->getcreatedAt()->format('d.m.Y H:m'),
            ];
        }

        if ($entity instanceof Recipe) {
            return [
                'id' => $entity->getId(),
                'titre' => $entity->getTitle(),
                'produit' => $entity->getProduct(),
                'note' => $this->ratingRepository->getAverageRating($entity),
                'contribution' => $entity->getIsContribution() ? 'oui' : 'non',
                'date_création' => $entity->getcreatedAt()->format('d.m.Y H:m'),
            ];
        }

        if ($entity instanceof Rating) {
            if ($entity->getProduct() !== null) {
                return [
                    'note_id' => $entity->getId(),
                    'date' => $entity->getCreatedAt()->format('d.m.Y H:m'),
                    'note' => $entity->getRating(),
                    'commentaire' => $entity->getComment() ?? 'Pas de commentaire',
                    'produit' => $entity->getProduct(),
                    'email' => $entity->getEmail(),
                    'pseudo' => $entity->getNickname(),
                ];
            }
    
            if ($entity->getRecipe() !== null) {
                return [
                    'note_id' => $entity->getId(),
                    'date' => $entity->getCreatedAt()->format('d.m.Y H:m'),
                    'note' => $entity->getRating(),
                    'commentaire' => $entity->getComment() ?? 'Pas de commentaire',
                    'recette' => $entity->getRecipe(),
                    'email' => $entity->getEmail(),
                    'pseudo' => $entity->getNickname(),
                ];
            }
        }
    }
}
