<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\FoodIngredients;
use App\Entity\Image;
use App\Entity\Label;
use App\Entity\NutritionalValues;
use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ProductUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $product = new Product();
        $datetime = new DateTime();
        $user = new User();
        $image = new Image();
        $category = new Category();
        $rating = new Rating();
        $recipe = new Recipe();
        $nutritionalValues = new NutritionalValues();
        $foodIngredient = new FoodIngredients();

        $product->setName('name')
                ->setDescription('description')
                ->setMainPicture('picture')
                ->setStatus(true)
                ->setSlug('slug')
                ->setCreatedAt($datetime)
                ->setUpdatedAt($datetime)
                ->setUser($user)
                ->addImage($image)
                ->setCategory($category)
                ->setDataSheet('dataSheet')
                ->addRating($rating)
                ->addRecipe($recipe)
                ->setNutritionalValues($nutritionalValues)
                ->addFoodIngredient($foodIngredient)
                ->setIsSeason(true);

        $this->assertTrue('name' === $product->getName());
        $this->assertTrue('description' === $product->getDescription());
        $this->assertTrue('picture' === $product->getMainPicture());
        $this->assertTrue(true === $product->getStatus());
        $this->assertTrue('slug' === $product->getSlug());
        $this->assertTrue($datetime === $product->getCreatedAt());
        $this->assertTrue($datetime === $product->getUpdatedAt());
        $this->assertTrue($user === $product->getUser());
        $this->assertContains($image, $product->getImages());
        $this->assertTrue($category === $product->getCategory());
        $this->assertTrue('dataSheet' === $product->getDataSheet());
        $this->assertContains($rating, $product->getRatings());
        $this->assertContains($recipe, $product->getRecipes());
        $this->assertTrue($nutritionalValues === $product->getNutritionalValues());
        $this->assertContains($foodIngredient, $product->getFoodIngredients());
        $this->assertTrue(true === $product->getIsSeason());

    }

    public function testIsFalse()
    {
        $product = new Product();
        $datetime = new DateTime();
        $user = new User();
        $image = new Image();
        $category = new Category();
        $rating = new Rating();
        $recipe = new Recipe();
        $nutritionalValues = new NutritionalValues();
        $foodIngredient = new FoodIngredients();

        $product->setName('name')
                ->setDescription('description')
                ->setMainPicture('picture')
                ->setStatus(true)
                ->setSlug('slug')
                ->setCreatedAt($datetime)
                ->setUpdatedAt($datetime)
                ->setUser($user)
                ->addImage($image)
                ->setCategory($category)
                ->setDataSheet('dataSheet')
                ->addRating($rating)
                ->addRecipe($recipe)
                ->setNutritionalValues($nutritionalValues)
                ->addFoodIngredient($foodIngredient)
                ->setIsSeason(true);

        $this->assertFalse('false' === $product->getName());
        $this->assertFalse('false' === $product->getDescription());
        $this->assertFalse('false' === $product->getMainPicture());
        $this->assertFalse(false === $product->getStatus());
        $this->assertFalse('false' === $product->getSlug());
        $this->assertFalse(new DateTime() === $product->getCreatedAt());
        $this->assertFalse(new DateTime() === $product->getUpdatedAt());
        $this->assertFalse(new User() === $product->getUser());
        $this->assertNotContains(new Image(), $product->getImages());
        $this->assertFalse(new Category() === $product->getCategory());
        $this->assertFalse('false' === $product->getDataSheet());
        $this->assertNotContains(new Rating(), $product->getRatings());
        $this->assertNotContains(new Recipe(), $product->getRecipes());
        $this->assertFalse(new NutritionalValues() === $product->getNutritionalValues());
        $this->assertNotContains(new FoodIngredients(), $product->getFoodIngredients());
        $this->assertFalse(false === $product->getIsSeason());
    }

    public function testIsEmpty()
    {
        $product = new Product();

        $this->assertEmpty($product->getName());
        $this->assertEmpty($product->getDescription());
        $this->assertEmpty($product->getMainPicture());
        $this->assertEmpty($product->getStatus());
        $this->assertEmpty($product->getSlug());
        $this->assertEmpty($product->getCreatedAt());
        $this->assertEmpty($product->getUpdatedAt());
        $this->assertEmpty($product->getUser());
        $this->assertEmpty($product->getImages());
        $this->assertEmpty($product->getCategory());
        $this->assertEmpty($product->getDataSheet());
        $this->assertEmpty($product->getRatings());
        $this->assertEmpty($product->getRecipes());
        $this->assertEmpty($product->getNutritionalValues());
        $this->assertEmpty($product->getFoodIngredients());
        $this->assertEmpty($product->getIsSeason());
    }
}
