<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastname', TextType::class, [
                'label' => 'Nom*',
                'attr' => [
                    'placeholder' => 'Votre nom',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom*',
                'attr' => [
                    'placeholder' => 'Votre prénom',
                ],
            ])
            ->add('company', TextType::class, [
                'label' => 'Société*',
                'attr' => [
                    'placeholder' => 'Nom de votre société',
                ],
            ])
            ->add('activity', ChoiceType::class, [
                'label' => 'Activité*',
                'placeholder' => '-- Sélectionner l\'activité --',
                'choices' => [
                    'Restaurant' => 'Restaurant',
                    'Traiteur' => 'Traiteur',
                    'Caviste' => 'Caviste',
                    'Epicerie Fine' => 'Epicerie Fine',
                    'Epicerie Générale' => 'Epicerie Générale',
                    'Autre' => 'Autre',
                ]
            ])
            ->add('position', TextType::class, [
                'label' => 'Fonction*',
                'attr' => [
                    'placeholder' => 'Poste au sein de l\'entreprise',
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail*',
                'attr' => [
                    'placeholder' => 'exemple@mail.fr',
                ],
            ])
            ->add('phone', TelType::class, [
                'label' => 'N° Téléphone',
                'required' => false,
                'attr' => [
                    'placeholder' => '0123456789',
                ],
            ])
            ->add('subject', ChoiceType::class, [
                'label' => 'Objet de la demande*',
                'placeholder' => '-- Sélectionner l\'objet du message --',
                'choices' => [
                    'Informations sur un/plusieurs produit(s)' => 'Informations sur un/plusieurs produit(s)',
                    'Devis/Prix' => 'Devis/Prix',
                    'Je souhaite être contacté par un commercial' => 'Je souhaite être contacté par un commercial',
                    'Autre' => 'Autre',
                ]
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Message*',
                'attr' => [
                    'rows' => 10,
                ]
            ])
            ->add('rgpd', CheckboxType::class, [
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
