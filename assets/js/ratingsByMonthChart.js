import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

// Importation des routes FOS JS Bundle
const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

const ctx = document.getElementById('ratingsPerMonth').getContext('2d');

const labels = [
    'Janv',
    'Fev',
    'Mars',
    'Avr',
    'Mai',
    'Juin',
    'Juil',
    'Aout',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
];

const ratingsPerMonth = new Chart(ctx, {
    type: 'line',
    data: {
        labels: labels,
        datasets: [{
            label: '# de Notes',
            data: [],
            backgroundColor: [
                'rgba(83, 104, 213, 0.2)',
            ],
            borderColor: [
                'rgba(83, 104, 213, 1)',
            ],
            borderWidth: 1.5
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true,
                ticks: {
                    stepSize: 1
                }
            }
        }
    }
});

async function fetchRatingsByProductData(id, year) {
    // const url =`https://127.0.0.1:8000/api/rating-stats/product/${id}/${year}`;
    const url = `${Routing.generate('api_product_rating_stats', { id: id, year: year })}`;

    const data = await fetch(url)
        .then(res => res.json())
        .then(Json => Json);

    // On récupère le nombre de votes par mois qu'on insère dans le tableau "ratings" 
    const ratings = [];
    for (let i = 1; i <= 12; i++) {
        ratings.push(data[i]);
    }

    // A chaque fetch on met à jour les données (nombre de votes par mois) du graphique
    ratingsPerMonth.config.data.datasets[0].data = ratings;
    ratingsPerMonth.update();
}

function handleSubmit(e) {
    e.preventDefault();

    const id = e.target[0].value;
    const year = e.target[1].value;

    fetchRatingsByProductData(id, year);
}

const ratingsByProductForm = document.getElementById('ratings_by_product');
ratingsByProductForm.addEventListener('submit', handleSubmit);
