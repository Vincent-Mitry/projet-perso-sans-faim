<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220207191005 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE about_us DROP FOREIGN KEY FK_B52303C3D07DA5AC');
        $this->addSql('CREATE TABLE story_telling_row (id INT AUTO_INCREMENT NOT NULL, about_us_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, content VARCHAR(255) NOT NULL, INDEX IDX_30540C2E7CE2CF2D (about_us_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE story_telling_row ADD CONSTRAINT FK_30540C2E7CE2CF2D FOREIGN KEY (about_us_id) REFERENCES about_us (id)');
        $this->addSql('DROP TABLE story_telling');
        $this->addSql('DROP INDEX UNIQ_B52303C3D07DA5AC ON about_us');
        $this->addSql('ALTER TABLE about_us DROP story_telling_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE story_telling (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE story_telling_row');
        $this->addSql('ALTER TABLE about_us ADD story_telling_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE about_us ADD CONSTRAINT FK_B52303C3D07DA5AC FOREIGN KEY (story_telling_id) REFERENCES story_telling (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B52303C3D07DA5AC ON about_us (story_telling_id)');
    }
}
