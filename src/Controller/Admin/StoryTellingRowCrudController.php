<?php

namespace App\Controller\Admin;

use App\Entity\StoryTellingRow;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class StoryTellingRowCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StoryTellingRow::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnDetail(),
            TextField::new('title'),
            TextareaField::new('content'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
                ->setEntityLabelInPlural('Qui sommes-nous');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->setPermissions([
                ACTION::EDIT => 'ROLE_ADMIN',
                ACTION::DELETE => 'ROLE_ADMIN',
                ACTION::NEW => 'ROLE_ADMIN',
                ACTION::BATCH_DELETE => 'ROLE_ADMIN'
            ])
        ;
    }
}
