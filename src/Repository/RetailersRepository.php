<?php

namespace App\Repository;

use App\Entity\Retailers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Retailers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Retailers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Retailers[]    findAll()
 * @method Retailers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RetailersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Retailers::class);
    }

    public function findAllHomepage()
    {
        return $this->createQueryBuilder('r')
                    ->where('r.displayHome = TRUE')
                    ->getQuery()
                    ->getResult();
    }

    public function findAll()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }
}
