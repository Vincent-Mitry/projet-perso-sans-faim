<?php

namespace App\Tests;

use App\Entity\GpsCoordinates;
use App\Entity\Retailers;
use PHPUnit\Framework\TestCase;

class GpsCoordinatesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $gps = new GpsCoordinates();
        $retailer = new Retailers();

        $gps->setLatitude(11.11111111)
            ->setLongitude(11.11111111)
            ->setRetailer($retailer);

        $this->assertEqualsWithDelta(11.11111111, $gps->getLatitude(), 0.000001);
        $this->assertEqualsWithDelta(11.11111111, $gps->getLongitude(), 0.000001);
        $this->assertTrue($retailer === $gps->getRetailer());
    }

    public function testIsFalse()
    {
        $gps = new GpsCoordinates();
        $retailer = new Retailers();

        $gps->setLatitude(11.11111111)
            ->setLongitude(11.11111111)
            ->setRetailer($retailer);

        $this->assertNotEqualsWithDelta(22.22222222, $gps->getLatitude(), 0.000001);
        $this->assertNotEqualsWithDelta(22.22222222, $gps->getLongitude(), 0.000001);
        $this->assertFalse(new Retailers() === $gps->getRetailer());
    }

    public function testIsEmpty()
    {
        $gps = new GpsCoordinates();

        $this->assertEmpty($gps->getLatitude());
        $this->assertEmpty($gps->getLongitude());
        $this->assertEmpty($gps->getRetailer());
    }
}
