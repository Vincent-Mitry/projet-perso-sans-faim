<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Rating;
use App\Form\RatingType;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\RatingRepository;
use App\Repository\RecipeRepository;
use App\Repository\SeasonDataRepository;
use App\Service\RatingService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class ProductController extends AbstractController
{
    /**
     * @Route("/produits", name="products_categories")
     */
    public function showAllCategories(
        CategoryRepository $categoryRepository,
        SeasonDataRepository $seasonDataRepository
    ): Response {
        return $this->render('product/categories.html.twig', [
            'categories' => $categoryRepository->findCategoriesToDisplay(),
            'seasonData' => $seasonDataRepository->findCurrentSeasonData(),
        ]);
    }

    /**
     * @Route("/produits/{category_slug}", name="products_by_category", priority=-1)
     * @ParamConverter("category", options={"mapping": {"category_slug": "slug"}})
     */
    public function showProductsByCategory(
        Category $category,
        ProductRepository $productRepository
    ): Response {
        return $this->render('product/products_by_category.html.twig', [
            'products' => $productRepository->findActiveProductsByCategory($category),
            'category' => $category,
        ]);
    }

    /**
     * @Route("/produits/saison", name="products_is_season")
     */
    public function showSeasonProducts(
        ProductRepository $productRepository,
        SeasonDataRepository $seasonDataRepository
    ): Response {
        return $this->render('product/products_is_season.html.twig', [
            'products' => $productRepository->findActiveSeasonProducts(),
            'seasonData' => $seasonDataRepository->findCurrentSeasonData(),
        ]);
    }

    /**
     * @Route("/produits/{category_slug}/{slug}", name="product_details", priority=-1)
     * @ParamConverter("category", options={"mapping": {"category_slug": "slug"}})
     * @ParamConverter("product", options={"mapping": {"slug": "slug"}})
     */
    public function detail(
        Product $product,
        ProductRepository $productRepository,
        Request $request,
        RatingService $ratingService,
        RatingRepository $ratingRepository,
        RecipeRepository $recipeRepository,
        RouterInterface $router
    ): Response {
        // Formulaire de notation
        $rating = new Rating();
        $form = $this->createForm(RatingType::class, $rating);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rating = $form->getData();
            $ratingService->persistRating($rating, $product);

            return $this->redirectToRoute('product_details', [
                'slug' => $product->getSlug(),
                'category_slug' => $product->getCategory()->getSlug(),
            ]);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $ratingService->displayRatingErrorMessage();
        }

        // Récupération des suggestions
        $category = $product->getCategory();
        $suggestions = $productRepository->findSuggestion($category, $product);

        // Récupération de la moyenne des notes
        $avgRating = $ratingRepository->getAverageRating($product);

        // Récupération des 3 derniers commentaires
        $lastThreeComments = $ratingRepository->findLastThreeComments($product);

        // Récupération des liens de recettes à afficher
        $recipesLinks = $recipeRepository->getRecipesLinksToDisplay($product);

        // Referer : information de routing
        $referer = $request->headers->get('referer');
        $refererPathInfo = Request::create($referer)->getPathInfo();
        $routeInfos = $router->match($refererPathInfo);
        $routeName = $routeInfos['_route'];

        return $this->render('product/product_details.html.twig', [
            'product' => $product,
            'suggestions' => $suggestions,
            'form' => $form->createView(),
            'avgRating' => $avgRating,
            'lastThreeComments' => $lastThreeComments,
            'recipesLinks' => $recipesLinks,
            'routeName' => $routeName,
        ]);
    }
}
