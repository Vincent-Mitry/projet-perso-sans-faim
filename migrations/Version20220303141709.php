<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220303141709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment_reply CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE recipe ADD email VARCHAR(255) DEFAULT NULL, ADD nickname VARCHAR(255) DEFAULT NULL, ADD is_contribution TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment_reply CHANGE created_at created_at DATETIME DEFAULT \'1970-01-02 00:00:00\' NOT NULL');
        $this->addSql('ALTER TABLE recipe DROP email, DROP nickname, DROP is_contribution');
    }
}
