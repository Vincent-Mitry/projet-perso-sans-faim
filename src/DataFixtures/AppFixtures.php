<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\FoodIngredients;
use App\Entity\GpsCoordinates;
use App\Entity\Ingredient;
use App\Entity\NutritionalValues;
use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use App\Entity\Retailers;
use App\Entity\SeasonData;
use App\Entity\StoryTellingRow;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    private $hasher;
    private $slugger;
    private $faker;
    private $manager;
    private $datetime;

    public function __construct(
        UserPasswordHasherInterface $hasher,
        SluggerInterface $slugger,
        EntityManagerInterface $manager
    ) {
        $this->hasher = $hasher;
        $this->slugger = $slugger;
        $this->faker = Factory::create('fr_FR');
        $this->manager = $manager;
        $this->datetime = new DateTime('now');
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // Création d'un User [ROLE_SUPER_ADMIN]
        $this->createUser('super_admin@test.com', ['ROLE_SUPER_ADMIN']);

        // Création d'un User [ROLE_ADMIN]
        $user = $this->createUser('admin@test.com', ['ROLE_ADMIN']);

        // Création d'un User [ROLE_MANAGER]
        $this->createUser('manager@test.com', ['ROLE_MANAGER']);

        // Création d'un User [ROLE_USER]
        $this->createUser('user@test.com', ['ROLE_USER']);

        // Création des catégories
        $categoryMeat = new Category();
        $categoryMeat->setName('viande')
                ->setSlug($this->slugger->slug($categoryMeat->getName()))
                ->setDescription($faker->text(400))
                ->setMainPicture('test_meat.png')
                ->setShortDescription($faker->text(100));

        $manager->persist($categoryMeat);

        $categoryFish = new Category();
        $categoryFish->setName('poisson')
                ->setSlug($this->slugger->slug($categoryFish->getName()))
                ->setDescription($faker->text(400))
                ->setMainPicture('test_fish.png')
                ->setShortDescription($faker->text(100));

        $manager->persist($categoryFish);

        $categoryVeggie = new Category();
        $categoryVeggie->setName('veggie')
                ->setSlug($this->slugger->slug($categoryVeggie->getName()))
                ->setDescription($faker->text(400))
                ->setMainPicture('test_veggie.png')
                ->setShortDescription($faker->text(100));

        $manager->persist($categoryVeggie);

        // Création des Produits
        // Viande
        for ($i = 0; $i < 4; ++$i) {
            $productMeat = new Product();
            $productMeat->setName($faker->words(3, true))
                    ->setDescription($faker->text(200))
                    ->setMainPicture('test_meat.png')
                    ->setStatus(true)
                    ->setSlug($this->slugger->slug($productMeat->getName()))
                    ->setCreatedAt(new DateTime())
                    ->setUser($user)
                    ->setCategory($categoryMeat)
                    ->setDataSheet('fiche-technique.pdf')
                    ->setIsSeason(false);

            $manager->persist($productMeat);

            // Création de 2 recettes
            for ($j = 0; $j < 2; ++$j) {
                $recipe = new Recipe();

                $recipe->setTitle($faker->words(4, true))
                        ->setDescription($faker->text(300))
                        ->setMainPicture('recette_image.png')
                        ->setSteps($faker->sentences(mt_rand(4, 9), false))
                        ->setSlug($this->slugger->slug($recipe->getTitle()))
                        ->setIsPublished(true)
                        ->setDisplayInProduct(true)
                        ->setCreatedAt(new DateTime())
                        ->setProduct($productMeat)
                        ->setServings(mt_rand(1, 6))
                        ->setIsContribution(false);

                $manager->persist($recipe);

                // Création des ingrédients
                for ($k = 0; $k < mt_rand(4, 8); ++$k) {
                    $ingredient = new Ingredient();

                    $ingredient->setName($faker->word())
                                ->setQuantity(mt_rand(100, 400))
                                ->setUnit('g')
                                ->setRecipe($recipe);

                    $manager->persist($ingredient);
                }

                $ingredient2 = new Ingredient();

                $ingredient2->setName($faker->word())
                            ->setQuantity(mt_rand(1, 3))
                            ->setRecipe($recipe);

                $manager->persist($ingredient2);

                // Création de notes et commentaires
                for ($m = 0; $m < mt_rand(3,5); ++$m) {
                    $rating = new Rating();
    
                    $rating->setRecipe($recipe)
                        ->setEmail($faker->email())
                        ->setRating(mt_rand(3, 5))
                        ->setComment($faker->text())
                        ->setIsPublished(true)
                        ->setRgpd(true)
                        ->setCreatedAt(new DateTime())
                        ->setNickname($faker->userName())
                        ->setDisplayHome(false)
                        ->setHasReply(false);
    
                    $manager->persist($rating);
                }
            }

            // Création de Valeurs nutritionnelles
            $nutritionalValues = new NutritionalValues();

            $nutritionalValues->setProduct($productMeat)
                            ->setEnergyKJ(mt_rand(300, 600))
                            ->setEnergyKcal(round($nutritionalValues->getEnergyKJ() * 0.2388))
                            ->setFat(round(mt_rand(20, 100) / 10, 1))
                            ->setFattyAcids(round($nutritionalValues->getFat() / 4, 1))
                            ->setCarbs(round(mt_rand(50, 120) / 10, 1))
                            ->setSugar(round($nutritionalValues->getCarbs() / 3, 1))
                            ->setProtein(round(mt_rand(30, 120) / 10, 1))
                            ->setSalt(mt_rand(10, 140) / 100);

            $manager->persist($nutritionalValues);

            // Création des Ingrédients alimentaires
            for ($l = 0; $l < mt_rand(8, 14); ++$l) {
                $foodIngredients = new FoodIngredients();

                $foodIngredients->setName($faker->word())
                                ->setPercentage($faker->optional(20)->numberBetween(1, 20))
                                ->setIsAllergen($faker->boolean(20))
                                ->setProduct($productMeat);

                $manager->persist($foodIngredients);
            }

            // Création des notes et commentaires
            for ($m = 0; $m < mt_rand(30, 50); ++$m) {
                $rating = new Rating();

                $rating->setProduct($productMeat)
                    ->setEmail($faker->email())
                    ->setRating(mt_rand(3, 5))
                    ->setComment($faker->text())
                    ->setIsPublished(true)
                    ->setRgpd(true)
                    ->setCreatedAt($faker->dateTimeBetween('-2 years', 'now'))
                    ->setNickname($faker->userName())
                    ->setDisplayHome($faker->optional(98, true)->boolean(false))
                    ->setHasReply(false);

                $manager->persist($rating);
            }
        }
        // Poisson
        for ($i = 0; $i < 4; ++$i) {
            $productFish = new Product();
            $productFish->setName($faker->words(3, true))
                    ->setDescription($faker->text(200))
                    ->setMainPicture('test_fish.png')
                    ->setStatus(true)
                    ->setSlug($this->slugger->slug($productFish->getName()))
                    ->setCreatedAt(new DateTime())
                    ->setUser($user)
                    ->setCategory($categoryFish)
                    ->setDataSheet('fiche-technique.pdf')
                    ->setIsSeason(false);

            $manager->persist($productFish);
        }
        // Veggie
        for ($i = 0; $i < 4; ++$i) {
            $productVeggie = new Product();
            $productVeggie->setName($faker->words(3, true))
                    ->setDescription($faker->text(200))
                    ->setMainPicture('test_veggie.png')
                    ->setStatus(true)
                    ->setSlug($this->slugger->slug($productVeggie->getName()))
                    ->setCreatedAt(new DateTime())
                    ->setUser($user)
                    ->setCategory($categoryVeggie)
                    ->setDataSheet('fiche-technique.pdf')
                    ->setIsSeason(false);

            $manager->persist($productVeggie);
        }
        // Saison
        $categoryItems = [
            $categoryFish,
            $categoryMeat,
            $categoryVeggie,
        ];

        for ($i = 0; $i < 4; ++$i) {
            $productSeason = new Product();
            $productSeason->setName($faker->words(3, true))
                    ->setDescription($faker->text(200))
                    ->setMainPicture('test_season.png')
                    ->setStatus(true)
                    ->setSlug($this->slugger->slug($productSeason->getName()))
                    ->setCreatedAt(new DateTime())
                    ->setUser($user)
                    ->setCategory($categoryItems[array_rand($categoryItems)])
                    ->setDataSheet('fiche-technique.pdf')
                    ->setIsSeason(true);

            $manager->persist($productSeason);
        }

        // Partenaires
        for ($i = 0; $i < 6; ++$i) {
            $retailers = new Retailers();
            $retailers->setName($faker->words(2, true))
                    ->setWebsite($faker->url())
                    ->setLogoImage('company_logo2.jpg')
                    ->setPhone($faker->phoneNumber())
                    ->setAddress($faker->address())
                    ->setCreatedAt(new DateTime())
                    ->setDisplayHome(true);

            $manager->persist($retailers);

            // Coordonnées GPS
            $gps = new GpsCoordinates();
            $gps->setLatitude($faker->randomFloat(10, 46, 50))
                ->setLongitude($faker->randomFloat(10, 1, 5))
                ->setRetailer($retailers);

            $manager->persist($gps);
        }

        // Données de Saison
        $seasonData = new SeasonData();
        $seasonData->setName('hiver 2022')
                ->setDescription($faker->text(400))
                ->setMainPicture('test_season.png')
                ->setSlug($this->slugger->slug($seasonData->getName()))
                ->setIsCurrent(true)
                ->setShortDescription($faker->text(100));

        $manager->persist($seasonData);

        // Données qui-sommes-nous

        for ($i = 1; $i < 4; ++$i) {
            $teamMember = new User();
            $teamMember->setEmail($faker->email())
            ->setPassword($this->hasher->hashPassword($teamMember, 'password'))
            ->setRoles(['ROLE_ADMIN'])
            ->setFirstname($faker->firstName())
            ->setLastname($faker->lastName())
            ->setPhone($faker->phoneNumber())
            ->setAvatar('girl.png')
            ->setDescription($faker->text(100))
            ->setDisplayAboutUs(true)
            ->setPosition($faker->words(2, true))
            ->setCreatedAt(new DateTime());

            $manager->persist($teamMember);
        }

        for ($j = 0; $j < 2; ++$j) {
            $storyTellingRow = new StoryTellingRow();
            $storyTellingRow->setTitle($faker->word())
                            ->setContent($faker->text(350));

            $manager->persist($storyTellingRow);
        }

        $gps1 = new GpsCoordinates();
        $gps1->setLatitude($faker->randomFloat(10, 46, 50))
            ->setLongitude($faker->randomFloat(10, 1, 5));

        $manager->persist($gps1);

        $manager->flush();
    }

    /**
     * Création d'un User
     *
     * @param string $email
     * @param array $roles
     * @return
     */
    public function createUser($email, $roles)
    {
        $user = new User();
        $user->setEmail($email)
            ->setRoles($roles)
            ->setFirstname($this->faker->firstName())
            ->setLastname($this->faker->lastName())
            ->setPhone($this->faker->phoneNumber())
            ->setDisplayAboutUs(false)
            ->setCreatedAt($this->datetime);

        $password = $this->hasher->hashPassword($user, 'password');
        $user->setPassword($password);

        $this->manager->persist($user);

        return $user;
    }
}
