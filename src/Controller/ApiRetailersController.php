<?php

namespace App\Controller;

use App\Entity\Retailers;
use App\Repository\RetailersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiRetailersController extends AbstractController
{
    /**
     * @Route("/api/retailers", name="api_retailers_all", methods={"GET"}, options={"expose"=true})
     */
    public function sendJsonRetailersInfo(RetailersRepository $retailersRepository): Response
    {
        return $this->json($retailersRepository->findAll(), 200, [], ['groups' => 'retailer:read']);
    }

    /**
     * @Route("/api/retailers/{id}", name="api_retailers_byId", methods={"GET"}, options={"expose"=true})
     */
    public function sendJsonRetailerInfoById($id, RetailersRepository $retailersRepository): Response
    {
        // Récupérer param id et le passer au find OneBy
        return $this->json($retailersRepository->findOneBy(['id' => $id]), 200, [], ['groups' => 'retailer:read']);
    }

    /**
     * @Route("/page-partenaire/{id}", name="retailer_page", options={"expose"=true})
     */
    public function pagePartenaire(Retailers $retailer)
    {
        return $this->render('retailer/retailer_page.html.twig', [
            'retailer' => $retailer,
        ]);
    }
}
