<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\RetailersRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=RetailersRepository::class)
 * @Vich\Uploadable
 */
class Retailers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("retailer:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("retailer:read")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("retailer:read")
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logoImage;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="retailers_images", fileNameProperty="logoImage")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("retailer:read")
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("retailer:read")
     */
    private $address;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $displayHome;

    /**
     * @ORM\OneToOne(targetEntity=GpsCoordinates::class, mappedBy="retailer", cascade={"persist", "remove"})
     * @Groups("retailer:read")
     */
    private $gpsCoordinates;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getLogoImage(): ?string
    {
        return $this->logoImage;
    }

    public function setLogoImage(?string $logoImage): self
    {
        $this->logoImage = $logoImage;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisplayHome(): ?bool
    {
        return $this->displayHome;
    }

    public function setDisplayHome(bool $displayHome): self
    {
        $this->displayHome = $displayHome;

        return $this;
    }

    public function getGpsCoordinates(): ?GpsCoordinates
    {
        return $this->gpsCoordinates;
    }

    public function setGpsCoordinates(?GpsCoordinates $gpsCoordinates): self
    {
        // unset the owning side of the relation if necessary
        if ($gpsCoordinates === null && $this->gpsCoordinates !== null) {
            $this->gpsCoordinates->setRetailer(null);
        }

        // set the owning side of the relation if necessary
        if ($gpsCoordinates !== null && $gpsCoordinates->getRetailer() !== $this) {
            $gpsCoordinates->setRetailer($this);
        }

        $this->gpsCoordinates = $gpsCoordinates;

        return $this;
    }
}
