<?php

namespace App\Tests;

use App\Entity\NutritionalValues;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class NutritionalValuesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $nutritionalValues = new NutritionalValues();
        $product = new Product();

        $nutritionalValues->setEnergyKJ(200)
             ->setEnergyKcal(100)
             ->setFat(10.1)
             ->setFattyAcids(1.1)
             ->setCarbs(10.10)
             ->setSugar(1.1)
             ->setProtein(10.1)
             ->setSalt(1.1)
             ->setProduct($product);

        $this->assertTrue(200 === $nutritionalValues->getEnergyKJ());
        $this->assertTrue(100 === $nutritionalValues->getEnergyKcal());
        $this->assertTrue(10.1 === $nutritionalValues->getFat());
        $this->assertTrue(1.1 === $nutritionalValues->getFattyAcids());
        $this->assertTrue(10.10 === $nutritionalValues->getCarbs());
        $this->assertTrue(1.1 === $nutritionalValues->getSugar());
        $this->assertTrue(10.1 === $nutritionalValues->getProtein());
        $this->assertTrue(1.1 === $nutritionalValues->getSalt());
        $this->assertTrue($nutritionalValues->getProduct() === $product);
    }

    public function testIsFalse()
    {
        $nutritionalValues = new NutritionalValues();
        $product = new Product();

        $nutritionalValues->setEnergyKJ(300)
             ->setEnergyKcal(100)
             ->setFat(10.1)
             ->setFattyAcids(1.1)
             ->setCarbs(10.1)
             ->setSugar(1.1)
             ->setProtein(10.1)
             ->setSalt(1.1)
             ->setProduct($product);

        $this->assertFalse(400 === $nutritionalValues->getEnergyKJ());
        $this->assertFalse(200 === $nutritionalValues->getEnergyKcal());
        $this->assertFalse(20 === $nutritionalValues->getFat());
        $this->assertFalse(2 === $nutritionalValues->getFattyAcids());
        $this->assertFalse(20 === $nutritionalValues->getCarbs());
        $this->assertFalse(2 === $nutritionalValues->getSugar());
        $this->assertFalse(20 === $nutritionalValues->getProtein());
        $this->assertFalse(2 === $nutritionalValues->getSalt());
        $this->assertFalse($nutritionalValues->getProduct() === new Product());
    }

    public function testIsEmpty()
    {
        $nutritionalValues = new NutritionalValues();

        $this->assertEmpty($nutritionalValues->getEnergyKJ());
        $this->assertEmpty($nutritionalValues->getEnergyKcal());
        $this->assertEmpty($nutritionalValues->getFat());
        $this->assertEmpty($nutritionalValues->getFattyAcids());
        $this->assertEmpty($nutritionalValues->getCarbs());
        $this->assertEmpty($nutritionalValues->getSugar());
        $this->assertEmpty($nutritionalValues->getProtein());
        $this->assertEmpty($nutritionalValues->getSalt());
        $this->assertEmpty($nutritionalValues->getProduct());
    }
}
