<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220208085938 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE story_telling_row DROP FOREIGN KEY FK_30540C2E7CE2CF2D');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6497CE2CF2D');
        $this->addSql('DROP TABLE about_us');
        $this->addSql('DROP INDEX IDX_30540C2E7CE2CF2D ON story_telling_row');
        $this->addSql('ALTER TABLE story_telling_row DROP about_us_id');
        $this->addSql('DROP INDEX IDX_8D93D6497CE2CF2D ON user');
        $this->addSql('ALTER TABLE user ADD display_about_us TINYINT(1) NOT NULL, DROP about_us_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE about_us (id INT AUTO_INCREMENT NOT NULL, video VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE story_telling_row ADD about_us_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE story_telling_row ADD CONSTRAINT FK_30540C2E7CE2CF2D FOREIGN KEY (about_us_id) REFERENCES about_us (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_30540C2E7CE2CF2D ON story_telling_row (about_us_id)');
        $this->addSql('ALTER TABLE user ADD about_us_id INT DEFAULT NULL, DROP display_about_us');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497CE2CF2D FOREIGN KEY (about_us_id) REFERENCES about_us (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_8D93D6497CE2CF2D ON user (about_us_id)');
    }
}
