<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Rating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * gets total number of products
     */
    public function countAllProducts()
    {
        return $this->createQueryBuilder('p')
                    ->select('count(p.id)')
                    ->getQuery()
                    ->getSingleScalarResult();
    }

    /**
     * Gets the total rating average from all products
     */
    public function totalAverageRating()
    {
        return $this->createQueryBuilder('p')
                    ->select('avg(r.rating) as ratingAvg, count(r.rating) as totalRates')
                    ->innerJoin(
                        Rating::class,
                        'r',
                        Join::WITH,
                        'p.id = r.product'
                    )
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    public function findAllHomepage()
    {
        return $this->createQueryBuilder('p')
                    ->where('p.status = TRUE')
                    // ->orderBy('p.season', 'DESC')
                    // ->addOrderBy('p.category', 'ASC')
                    ->orderBy('p.isSeason', 'DESC')
                    ->addorderBy('p.createdAt', 'DESC')
                    ->getQuery()
                    ->getResult();
    }

    public function findSuggestion(Category $category, Product $product)
    {
        return $this->createQueryBuilder('p')
                    ->where('p.status = TRUE')
                    ->andWhere('p != (:product)')
                    ->andWhere('p.category IN (:category)')
                    ->orderBy('p.isSeason', 'DESC')
                    ->addOrderBy('RAND()')
                    ->setMaxResults(3)
                    ->setParameters([
                        'category' => $category,
                        'product' => $product,
                    ])
                    ->getQuery()
                    ->getResult();
    }

    public function findActiveProductsByCategory(Category $category)
    {
        return $this->createQueryBuilder('p')
                    ->where('p.status = TRUE')
                    ->andWhere('p.category IN (:category)')
                    ->orderBy('p.isSeason', 'DESC')
                    ->addorderBy('p.createdAt', 'DESC')
                    ->setParameter('category', $category)
                    ->getQuery()
                    ->getResult();
    }

    public function findActiveSeasonProducts()
    {
        return $this->createQueryBuilder('p')
                    ->where('p.status = TRUE')
                    ->andWhere('p.isSeason = TRUE')
                    ->orderBy('p.createdAt', 'DESC')
                    ->getQuery()
                    ->getResult();
    }
}
