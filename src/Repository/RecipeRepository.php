<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Recipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recipe[]    findAll()
 * @method Recipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recipe::class);
    }

    public function countAllRecipes()
    {
        return $this->createQueryBuilder('r')
                    ->select('count(r.id)')
                    ->getQuery()
                    ->getSingleScalarResult();
    }

    /**
     * Gets the total rating average from all recipes
     */
    public function totalAverageRating()
    {
        return $this->createQueryBuilder('e')
                    ->select('avg(r.rating) as ratingAvg, count(r.rating) as totalRates')
                    ->innerJoin(
                        Rating::class,
                        'r',
                        Join::WITH,
                        'e.id = r.recipe'
                    )
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    public function getRecipesLinksToDisplay(Product $product)
    {
        return $this->createQueryBuilder('r')
                    ->where('r.product IN (:product)')
                    ->andWhere('r.isPublished = TRUE')
                    ->andWhere('r.displayInProduct = TRUE')
                    ->setParameter('product', $product)
                    ->getQuery()
                    ->getResult();
    }

    public function findPublishedRecipesByProduct(Product $product)
    {
        return $this->createQueryBuilder('r')
                    ->where('r.isPublished = TRUE')
                    ->andWhere('r.product IN (:product)')
                    ->addorderBy('r.createdAt', 'DESC')
                    ->setParameter('product', $product)
                    ->getQuery()
                    ->getResult();
    }
}
