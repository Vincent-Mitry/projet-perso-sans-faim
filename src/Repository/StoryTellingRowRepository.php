<?php

namespace App\Repository;

use App\Entity\StoryTellingRow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StoryTellingRow|null find($id, $lockMode = null, $lockVersion = null)
 * @method StoryTellingRow|null findOneBy(array $criteria, array $orderBy = null)
 * @method StoryTellingRow[]    findAll()
 * @method StoryTellingRow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StoryTellingRowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StoryTellingRow::class);
    }

    // /**
    //  * @return StoryTellingRow[] Returns an array of StoryTellingRow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StoryTellingRow
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
