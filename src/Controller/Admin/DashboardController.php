<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\CommentReply;
use App\Entity\Product;
use App\Entity\Rating;
use App\Entity\Recipe;
use App\Entity\Retailers;
use App\Entity\SeasonData;
use App\Entity\StoryTellingRow;
use App\Entity\User;
use App\Form\CommentReplyType;
use App\Form\RatingsByProductChartType;
use App\Repository\ProductRepository;
use App\Repository\RatingRepository;
use App\Repository\RecipeRepository;
use App\Service\CommentReplyService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    private $ratingRepository;
    private $recipeRepository;
    private $productRepository;

    public function __construct(
        RatingRepository $ratingRepository,
        RecipeRepository $recipeRepository,
        ProductRepository $productRepository
    ) {
        $this->ratingRepository = $ratingRepository;
        $this->recipeRepository = $recipeRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $topProducts = $this->ratingRepository->getTopThreeRatedItems(Product::class);
        $lastProducts = $this->ratingRepository->getLastThreeRatedItems(Product::class);
        $topRecipes = $this->ratingRepository->getTopThreeRatedItems(Recipe::class);
        $lastRecipes = $this->ratingRepository->getLastThreeRatedItems(Recipe::class);

        $ratingsByProductForm = $this->createForm(RatingsByProductChartType::class);

        return $this->render('admin/dashboard.html.twig', [
            'topProducts' => $topProducts,
            'lastProducts' => $lastProducts,
            'topRecipes' => $topRecipes,
            'lastRecipes' => $lastRecipes,
            'countRatedProducts' => $this->ratingRepository->countRatedItems(Product::class),
            'countRatedRecipes' => $this->ratingRepository->countRatedItems(Recipe::class),
            'ratingsByProductForm' => $ratingsByProductForm->createView(),
        ]);
    }

    /**
     * @Route("/admin/stats/notes/{entity}", name="average_ratings_all")
     */
    public function showAllAverageRatings($entity) {
        if ($entity === 'produits') {
            $entityRatingList = $this->ratingRepository->getAllRatedItems(Product::class);
            $entityTotalAverage = $this->productRepository->totalAverageRating();
            $countRatedItems = $this->ratingRepository->countRatedItems(Product::class);
            $countAllItems = $this->productRepository->countAllProducts();
        }

        if ($entity === 'recettes') {
            $entityRatingList = $this->ratingRepository->getAllRatedItems(Recipe::class);
            $entityTotalAverage = $this->recipeRepository->totalAverageRating();
            $countRatedItems = $this->ratingRepository->countRatedItems(Recipe::class);
            $countAllItems = $this->recipeRepository->countAllRecipes();
        }

        return $this->render('admin/average_ratings_all.html.twig', [
            'entity' => $entity,
            'entityRatingList' => $entityRatingList,
            'entityTotal' => $entityTotalAverage,
            'countRatedItems' => $countRatedItems,
            'countAllItems' => $countAllItems,
        ]);
    }

    /**
     * @Route("/admin/reponse-commentaire/{id}", name="reply_comment")
     */
    public function replyComment(
        Request $request,
        RatingRepository $ratingRepository,
        CommentReplyService $commentReplyService
    ): Response {
        $ratingId = $request->attributes->get('id');
        $rating = $ratingRepository->findOneBy(['id' => $ratingId]);
        
        $commentReply = new CommentReply();
        $form = $this->createForm(CommentReplyType::class, $commentReply, [
            'rating' => $rating
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentReply = $form->getData();
            $commentReplyService->persistCommentReply($commentReply, $rating);

            return $this->redirectToRoute('admin');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $commentReplyService->displayCommentReplyErrorMessage();
        }


        return $this->render('admin/reply_comment.html.twig', [
            'form' => $form->createView(),
            'rating' => $rating,
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Sans Faim');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Catégories', 'fa fa-list', Category::class);
        yield MenuItem::linkToCrud('Produits', 'fa fa-cubes', Product::class);
        yield MenuItem::linkToCrud('Recettes', 'fa fa-cutlery', Recipe::class);
        yield MenuItem::linkToCrud('Saison Info', 'fa fa-leaf', SeasonData::class);
        yield MenuItem::subMenu('Notes et commentaires', 'fa fa-commenting-o')->setSubItems([
            MenuItem::linkToCrud('Produits', 'fa fa-cubes', Rating::class)
                ->setController(RatingProductCrudController::class),
            MenuItem::linkToCrud('Recettes', 'fa fa-cutlery', Rating::class)
            ->setController(RatingRecipeCrudController::class),
        ]);
        yield MenuItem::linkToCrud('Partenaires', 'fa fa-handshake-o', Retailers::class);
        yield MenuItem::linkToCrud('Qui sommes-nous', 'fa fa-info-circle', StoryTellingRow::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Utilisateurs', 'fa fa-user', User::class)->setPermission('ROLE_ADMIN');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
            ->addMenuItems(([
                MenuItem::linkToRoute('Modifier mot de passe', 'fa fa-key', 'change_password')
            ]));
    }
}
